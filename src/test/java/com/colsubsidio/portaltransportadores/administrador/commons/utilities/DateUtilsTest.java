package com.colsubsidio.portaltransportadores.administrador.commons.utilities;

import com.colsubsidio.portaltransportadores.administrador.commons.enums.EDateFormat;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class DateUtilsTest {
    DateUtils dateUtils = new DateUtils();

    @Test
    void getDateString() {
        String date = dateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat());
        Assertions.assertNotEquals("", date);
    }

    @Test
    void getDateStringEmpty() {
        String date = dateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat());
        Assertions.assertTrue(!date.isEmpty());
    }
}