package com.colsubsidio.portaltransportadores.administrador.commons.utilities;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.ReservationCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.Reservation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ParsingHistoryUtilsTest {

    @Test
    void dtoToEntity() {
        ReservationCreateReqDto reservationCreateReqDto = new ReservationCreateReqDto(
                "8fc90f01-cfe3-4f01-8da2-c36aa3c930fc",
                "345",
                "12000.80",
                "123456789",
                "2021-04-10"
        );
        Reservation reservationEntity = ParsingHistoryUtils.dtoToEntity(reservationCreateReqDto, "0987654321");
        Assertions.assertEquals("8fc90f01-cfe3-4f01-8da2-c36aa3c930fc", reservationEntity.getPartitionKey());
        Assertions.assertEquals("345", reservationEntity.getIdReserva());
        Assertions.assertEquals("123456789", reservationEntity.getNumeroDocumentoCliente());
        Assertions.assertTrue(reservationEntity.getFechaEliminado().isEmpty());
        Assertions.assertTrue(reservationEntity.getFechaIda().isEmpty());
        Assertions.assertTrue(reservationEntity.getFechaRegreso().isEmpty());
        Assertions.assertFalse(reservationEntity.getFechaCreado().isEmpty());
    }
}