package com.colsubsidio.portaltransportadores.administrador.business;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.*;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EDateFormat;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.ETypeCode;
import com.colsubsidio.portaltransportadores.administrador.commons.handler.ResponseHandler;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.Reservation;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.DateUtils;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.GeneralUtils;
import com.colsubsidio.portaltransportadores.administrador.integrations.dao.repository.ReservationRepository;
import com.colsubsidio.portaltransportadores.administrador.integrations.dao.services.ReservationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

class ReservationBusinessTest {

    ReservationRepository reservationRepositoryMock = Mockito.mock(ReservationRepository.class);
    ResponseHandler responseHandler = new ResponseHandler();
    GeneralUtils generalUtils = new GeneralUtils(responseHandler);
    ReservationService reservationServiceMock = Mockito.mock(ReservationService.class);

    ReservationBusiness reservationBusiness = new ReservationBusiness(reservationRepositoryMock, responseHandler, generalUtils, reservationServiceMock);

    @BeforeEach
    void setUp() {

        List<ReservationCreateReqDto> reservationCreateReqDtoList = new ArrayList<>();
        ReservationCreateReqDto reservationCreateReqDto = new ReservationCreateReqDto(
                "8fc90f01-cfe3-4f01-8da2-c36aa3c930fc",
                "345",
                "12000.80",
                "123456789",
                "2021-04-10"
        );
        reservationCreateReqDtoList.add(reservationCreateReqDto);


        List<Reservation> reservationEntityList = new ArrayList<>();
        Reservation reservationEntity = new Reservation(
                "4f01-8da2-c36aa3c930fc",
                "8fc90f01-cfe3-4f01-8da2-c36aa3c930fc",
                "3faef003-0bde-462e-b6b5-f4a05bc8896e",
                "22481",
                "12000.80",
                "123456",
                "654321",
                "2021-04-10",
                DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()),
                DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()),
                DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()),
                DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()),
                "0987654321"
        );
        reservationEntityList.add(reservationEntity);

        ReservationRespDto reservationRespDto = new ReservationRespDto();
        TicketRespDto ticketRespDto = new TicketRespDto(
                "BOG-27383401",
                "PLAN PISCITOUR",
                ""
        );
        ClientRespDto clientRespDto = new ClientRespDto(
                "Martinez",
                "Pedro",
                "Perez",
                "Jose",
                "123456"
        );
        RouteAvailabilityRespDto routeAvailabilityRespDto = new RouteAvailabilityRespDto(
                true,
                true
        );
        reservationRespDto.setReserva(ticketRespDto);
        reservationRespDto.setCliente(clientRespDto);
        reservationRespDto.setDisponibilidadTrayecto(routeAvailabilityRespDto);

        Mockito.doCallRealMethod().when(reservationRepositoryMock).saveHistory(reservationCreateReqDtoList);
        Mockito.when(reservationRepositoryMock.getHistory()).thenReturn(reservationEntityList);
        Mockito.when(reservationRepositoryMock.getHistoryByDocumentNumber("123456789")).thenReturn(reservationEntityList);
        Mockito.when(reservationRepositoryMock.getHistoryByIdReservation("235")).thenReturn(reservationEntityList);
        Mockito.when(reservationRepositoryMock.getHistoryByIdReservation("22481")).thenReturn(reservationEntityList);
        Mockito.when(reservationRepositoryMock.getHistoryByIdReservationAndCurrentDate("22481")).thenReturn(reservationEntityList);
        Mockito.when(reservationRepositoryMock.delete("3faef003-0bde-462e-b6b5-f4a05bc8896e")).thenReturn(reservationEntity);
        Mockito.when(reservationRepositoryMock.getHistoryByRowKey("3faef003-0bde-462e-b6b5-f4a05bc8896e")).thenReturn(reservationEntity);
        Mockito.when(reservationServiceMock.obtainReservation(reservationEntityList)).thenReturn(reservationRespDto);

    }

    @Test
    void obtain() {
        ResponseDto responseDto = reservationBusiness.obtain();
        List<?> historyEntityList = (List<?>) responseDto.getData();
        Reservation reservationEntity = (Reservation) historyEntityList.get(0);
        Assertions.assertEquals(Reservation.class, historyEntityList.get(0).getClass());
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", reservationEntity.getRowKey());
        Assertions.assertEquals(1, historyEntityList.size());
        Assertions.assertEquals(HttpStatus.OK.value(), responseDto.getResultado().get(0).getCodigo());

    }

    @Test
    void obtainByRowKey() {
        ResponseDto responseDto = reservationBusiness.obtain("3faef003-0bde-462e-b6b5-f4a05bc8896e");
        Reservation reservationEntity = (Reservation) responseDto.getData();
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", reservationEntity.getRowKey());
        Assertions.assertEquals(Reservation.class, reservationEntity.getClass());
        Assertions.assertEquals(HttpStatus.OK.value(), responseDto.getResultado().get(0).getCodigo());

    }

    @Test
    void obtainReservation() {
        ResponseDto responseDto = reservationBusiness.obtainReservation(ETypeCode.DOCUMENT.getCode(), "123456789");
        List<?> historyEntityList = (List<?>) responseDto.getData();
        Reservation reservationEntity = (Reservation) historyEntityList.get(0);
        Assertions.assertEquals(Reservation.class, historyEntityList.get(0).getClass());
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", reservationEntity.getRowKey());
        Assertions.assertEquals(1, historyEntityList.size());
        Assertions.assertEquals(HttpStatus.OK.value(), responseDto.getResultado().get(0).getCodigo());

    }

    @Test
    void obtainReservation1() {
        ResponseDto responseDto = reservationBusiness.obtainReservation(ETypeCode.RESERVATION.getCode(), "235");
        List<?> historyEntityList = (List<?>) responseDto.getData();
        Reservation reservationEntity = (Reservation) historyEntityList.get(0);
        Assertions.assertEquals(Reservation.class, historyEntityList.get(0).getClass());
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", reservationEntity.getRowKey());
        Assertions.assertEquals(1, historyEntityList.size());
        Assertions.assertEquals(HttpStatus.OK.value(), responseDto.getResultado().get(0).getCodigo());

    }

    @Test
    void obtainTicket() {
        ResponseDto responseDto = reservationBusiness.obtainTicket(ETypeCode.RESERVATION.getCode(), "22481");
        ReservationRespDto reservationRespDto = (ReservationRespDto) responseDto.getData();
        Assertions.assertEquals(ReservationRespDto.class, reservationRespDto.getClass());
        Assertions.assertEquals("BOG-27383401", reservationRespDto.getReserva().getCodigo());
        Assertions.assertEquals(HttpStatus.OK.value(), responseDto.getResultado().get(0).getCodigo());
    }

    @Test
    void save() {
        List<ReservationCreateReqDto> reservationCreateReqDtoList = new ArrayList<>();
        ReservationCreateReqDto reservationCreateReqDto = new ReservationCreateReqDto(
                "8fc90f01-cfe3-4f01-8da2-c36aa3c930fc",
                "345",
                "12000.80",
                "123456789",
                "2021-04-10"
        );
        reservationCreateReqDtoList.add(reservationCreateReqDto);

        ResponseDto responseDto = reservationBusiness.save(reservationCreateReqDtoList);
        Assertions.assertEquals(HttpStatus.OK.value(), responseDto.getResultado().get(0).getCodigo());

        Mockito.doCallRealMethod().when(reservationRepositoryMock).saveHistory(null);
        ResponseDto responseDtoThrow = reservationBusiness.save(null);
        Assertions.assertEquals(HttpStatus.MULTI_STATUS.value(), responseDtoThrow.getResultado().get(0).getCodigo());
    }

    @Test
    void saveErrorDate() {
        List<ReservationCreateReqDto> reservationCreateReqDtoList = new ArrayList<>();
        ReservationCreateReqDto reservationCreateReqDto = new ReservationCreateReqDto(
                "8fc90f01-cfe3-4f01-8da2-c36aa3c930fc",
                "345",
                "12000.80",
                "123456789",
                "0000-04-10"
        );
        reservationCreateReqDtoList.add(reservationCreateReqDto);
        Mockito.doCallRealMethod().when(reservationRepositoryMock).saveHistory(reservationCreateReqDtoList);

        ResponseDto responseDtoValidate = reservationBusiness.save(reservationCreateReqDtoList);
        Assertions.assertEquals(HttpStatus.NOT_ACCEPTABLE.value(), responseDtoValidate.getResultado().get(0).getCodigo());
    }

    @Test
    void delete() {
        ResponseDto responseDto = reservationBusiness.delete("3faef003-0bde-462e-b6b5-f4a05bc8896e");
        Reservation reservationEntity = (Reservation) responseDto.getData();
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", reservationEntity.getRowKey());
        Assertions.assertFalse(reservationEntity.getFechaEliminado().isEmpty());
        Assertions.assertEquals(HttpStatus.OK.value(), responseDto.getResultado().get(0).getCodigo());

    }

    @Test
    void update() {
        ReservationUpdateReqDto reservationUpdateReqDto = new ReservationUpdateReqDto(
                "123456",
                "12345",
                "IDA"
        );

        List<Reservation> reservationEntityList = new ArrayList<>();
        Reservation reservationEntity = new Reservation(
                "3faef003-0bde",
                "8fc90f01-cfe3-4f01-8da2-c36aa3c930fc",
                "3faef003-0bde-462e-b6b5-f4a05bc8896e",
                "22481",
                "12000.80",
                "123456",
                "12345",
                "2021-04-10",
                DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()),
                DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()),
                DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()),
                DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()),
                "0987654321"
        );
        reservationEntityList.add(reservationEntity);


        Mockito.when(reservationRepositoryMock.updateReservation(reservationUpdateReqDto)).thenReturn(reservationEntityList);

        ResponseDto responseDto = reservationBusiness.update(reservationUpdateReqDto);
        List<?> historyEntityList = (List<?>) responseDto.getData();
        Reservation reservation = (Reservation) historyEntityList.get(0);
        Assertions.assertEquals(Reservation.class, historyEntityList.get(0).getClass());
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", reservation.getRowKey());
        Assertions.assertEquals(1, historyEntityList.size());
        Assertions.assertEquals(HttpStatus.OK.value(), responseDto.getResultado().get(0).getCodigo());

    }
}