package com.colsubsidio.portaltransportadores.administrador.business;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.ResponseDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.UserCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.UserUpdateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EDateFormat;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EDocumentType;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EProfilesUser;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EState;
import com.colsubsidio.portaltransportadores.administrador.commons.handler.ResponseHandler;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.User;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.DateUtils;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.GeneralUtils;
import com.colsubsidio.portaltransportadores.administrador.integrations.dao.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

class UserBusinessTest {

    UserRepository userRepositoryMock = Mockito.mock(UserRepository.class);
    ResponseHandler responseHandler = new ResponseHandler();
    GeneralUtils generalUtils = new GeneralUtils(responseHandler);
    UserBusiness userBusiness = new UserBusiness(userRepositoryMock, responseHandler, generalUtils);

    @BeforeEach
    void setUp() {
        UserCreateReqDto userCreateReqDto = new UserCreateReqDto(
                "123456789",
                "pepito@gmail.com",
                "Pepito Perez",
                "1065638573");
        List<User> iUser = new ArrayList<>();
        User user = new User();
        user.setUsuario("123456789");
        user.setClave("123456789");
        user.setNumeroDocumento("123456789");
        user.setTipoDocumento(EDocumentType.CC.getCode());
        user.setCorreo("pepito@gmail.com");
        user.setNombreCompleto("Pepito Perez");
        user.setPerfil(EProfilesUser.TRANSPORTADOR.getCode());
        user.setEstado(EState.ACTIVE.getCode());
        user.setNitEmpresa("1065638573");
        user.setPartitionKey("usuario");
        user.setRowKey("3faef003-0bde-462e-b6b5-f4a05bc8896e");
        user.setFechaEliminado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        user.setFechaActualizado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        user.setFechaCreado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));

        iUser.add(user);
        Mockito.doCallRealMethod().when(userRepositoryMock).saveUser(userCreateReqDto);
        Mockito.when(userRepositoryMock.getUsers("1065638573")).thenReturn(iUser);
        Mockito.when(userRepositoryMock.getUserByRowKey("3faef003-0bde-462e-b6b5-f4a05bc8896e")).thenReturn(user);
        Mockito.when(userRepositoryMock.deleteUser("3faef003-0bde-462e-b6b5-f4a05bc8896e")).thenReturn(user);
    }

    @Test
    void obtain() {
        ResponseDto responseDto = userBusiness.obtain("1065638573");
        List<?> userEntityList = (List<?>) responseDto.getData();
        User userEntity = (User) userEntityList.get(0);
        Assertions.assertEquals(User.class, userEntityList.get(0).getClass());
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", userEntity.getRowKey());
        Assertions.assertEquals(1, userEntityList.size());
        Assertions.assertEquals(200, responseDto.getResultado().get(0).getCodigo());

    }

    @Test
    void obtainByRowKey() {
        ResponseDto responseDto = userBusiness.obtainByRowKey("3faef003-0bde-462e-b6b5-f4a05bc8896e");
        User userEntity = (User) responseDto.getData();
        Assertions.assertEquals(User.class, userEntity.getClass());
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", userEntity.getRowKey());
        Assertions.assertEquals(200, responseDto.getResultado().get(0).getCodigo());

    }

    @Test
    void save() {
        UserCreateReqDto userCreateReqDto = new UserCreateReqDto(
                "123456789",
                "pepito@gmail.com",
                "Pepito Perez",
                "1065638573");
        ResponseDto responseDto = userBusiness.save(userCreateReqDto);
        Assertions.assertEquals(200, responseDto.getResultado().get(0).getCodigo());

        Mockito.doCallRealMethod().when(userRepositoryMock).saveUser(null);
        ResponseDto responseDtoThrow = userBusiness.save(null);
        Assertions.assertEquals(207, responseDtoThrow.getResultado().get(0).getCodigo());
    }

    @Test
    void update() {
        UserUpdateReqDto userUpdateReqDto = new UserUpdateReqDto(
                "usuario",
                "3faef003-0bde-462e-b6b5-f4a05bc8896e",
                "123456789",
                "pepito@gmail.com",
                "Pepito Perez",
                "1065638573");

        User user = new User();
        user.setUsuario("123456789");
        user.setClave("123456789");
        user.setNumeroDocumento("123456789");
        user.setTipoDocumento(EDocumentType.CC.getCode());
        user.setCorreo("pepito@gmail.com");
        user.setNombreCompleto("Pepito Perez");
        user.setPerfil(EProfilesUser.TRANSPORTADOR.getCode());
        user.setEstado(EState.ACTIVE.getCode());
        user.setNitEmpresa("1065638573");
        user.setPartitionKey("usuario");
        user.setRowKey("3faef003-0bde-462e-b6b5-f4a05bc8896e");
        user.setFechaEliminado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        user.setFechaActualizado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        user.setFechaCreado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        Mockito.when(userRepositoryMock.updateUser(userUpdateReqDto)).thenReturn(user);

        Assertions.assertEquals("Cedula de ciudadanía", EDocumentType.CC.getDetail());

        ResponseDto responseDto = userBusiness.update(userUpdateReqDto);
        User userEntity = (User) responseDto.getData();
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", userEntity.getRowKey());
        Assertions.assertEquals(200, responseDto.getResultado().get(0).getCodigo());

    }

    @Test
    void delete() {
        ResponseDto responseDto = userBusiness.delete("3faef003-0bde-462e-b6b5-f4a05bc8896e");
        User userEntity = (User) responseDto.getData();
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", userEntity.getRowKey());
        Assertions.assertFalse(userEntity.getFechaEliminado().isEmpty());
        Assertions.assertEquals(200, responseDto.getResultado().get(0).getCodigo());

    }
}