package com.colsubsidio.portaltransportadores.administrador.business;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.BusinessCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.BusinessUpdateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ResponseDto;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EDateFormat;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EDocumentType;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EProfilesUser;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EState;
import com.colsubsidio.portaltransportadores.administrador.commons.handler.ResponseHandler;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.User;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.DateUtils;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.GeneralUtils;
import com.colsubsidio.portaltransportadores.administrador.integrations.dao.repository.BusinessRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;


class BusinessBusinessTest {

    ResponseHandler responseHandler = new ResponseHandler();
    BusinessRepository businessRepositoryMock = Mockito.mock(BusinessRepository.class);
    GeneralUtils generalUtils = new GeneralUtils(responseHandler);
    BusinessBusiness businessBusiness = new BusinessBusiness(businessRepositoryMock, responseHandler, generalUtils);

    @BeforeEach
    void setUp() {
        BusinessCreateReqDto createBusiness = new BusinessCreateReqDto(
                "123456789",
                "edwinferreira92@gmail.com",
                "Mock Prueba");

        List<User> iUser = new ArrayList<>();
        User user = new User();
        user.setUsuario("123456789");
        user.setClave("Rf78hJ");
        user.setNumeroDocumento("123456789");
        user.setTipoDocumento(EDocumentType.NIT.getCode());
        user.setCorreo("edwinferreira92@gmail.com");
        user.setNombreCompleto("Transportes SAS");
        user.setPerfil(EProfilesUser.COMPANY.getCode());
        user.setEstado(EState.ACTIVE.getCode());
        user.setNitEmpresa("");
        user.setPartitionKey("empresa");
        user.setRowKey("3faef003-0bde-462e-b6b5-f4a05bc8896e");
        user.setFechaEliminado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        user.setFechaActualizado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        user.setFechaCreado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));

        iUser.add(user);
        Mockito.doCallRealMethod().when(businessRepositoryMock).saveBusiness(createBusiness);
        Mockito.when(businessRepositoryMock.getBusiness()).thenReturn(iUser);
        Mockito.when(businessRepositoryMock.getBusinessByRowKey("3faef003-0bde-462e-b6b5-f4a05bc8896e")).thenReturn(user);
        Mockito.when(businessRepositoryMock.deleteBusiness("3faef003-0bde-462e-b6b5-f4a05bc8896e")).thenReturn(user);
    }


    @Test
    void obtain() {
        ResponseDto responseDto = businessBusiness.obtain();
        List<User> userEntityList = (List<User>) responseDto.getData();
        Assertions.assertEquals(User.class, userEntityList.get(0).getClass());
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", userEntityList.get(0).getRowKey());
        Assertions.assertEquals(userEntityList.size(), 1);
        Assertions.assertEquals(200, responseDto.getResultado().get(0).getCodigo());

    }

    @Test
    void obtainByRowKey() {
        ResponseDto responseDto = businessBusiness.obtainByRowKey("3faef003-0bde-462e-b6b5-f4a05bc8896e");
        User userEntity = (User) responseDto.getData();
        Assertions.assertEquals(User.class, userEntity.getClass());
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", userEntity.getRowKey());
        Assertions.assertEquals(200, responseDto.getResultado().get(0).getCodigo());

    }

    @Test
    void saveBusiness() {
        BusinessCreateReqDto createBusiness = new BusinessCreateReqDto(
                "123456789",
                "edwinferreira92@gmail.com",
                "Mock Prueba");
        ResponseDto responseDto = businessBusiness.saveBusiness(createBusiness);
        Assertions.assertEquals(200, responseDto.getResultado().get(0).getCodigo());

        BusinessCreateReqDto createBusinessThrow = new BusinessCreateReqDto(
                "123456789",
                "edwinferreira92@gmail",
                "Mock Prueba");
        ResponseDto responseDtoThrow = businessBusiness.saveBusiness(createBusinessThrow);
        Assertions.assertTrue(responseDtoThrow.getResultado().size() > 1);
    }

    @Test
    void update() {
        BusinessUpdateReqDto updateBusiness = new BusinessUpdateReqDto(
                "empresa",
                "a0e8f1c1-3cec-4f60-bc1e-eb1164791745",
                "123456789",
                "edwinferreira92@gmail.com",
                "Mock Prueba");
        User user = new User();
        user.setUsuario("123456789");
        user.setClave("Rf78hJ");
        user.setNumeroDocumento("123456789");
        user.setTipoDocumento(EDocumentType.NIT.getCode());
        user.setCorreo("edwinferreira92@gmail.com");
        user.setNombreCompleto("Transportes SAS");
        user.setPerfil(EProfilesUser.COMPANY.getCode());
        user.setEstado(EState.ACTIVE.getCode());
        user.setNitEmpresa("");
        user.setPartitionKey("empresa");
        user.setRowKey("3faef003-0bde-462e-b6b5-f4a05bc8896e");
        user.setFechaEliminado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        user.setFechaActualizado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        user.setFechaCreado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        Mockito.when(businessRepositoryMock.updateBusiness(updateBusiness)).thenReturn(user);

        ResponseDto responseDto = businessBusiness.update(updateBusiness);
        User userEntity = (User) responseDto.getData();
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", userEntity.getRowKey());
        Assertions.assertEquals(200, responseDto.getResultado().get(0).getCodigo());

        BusinessUpdateReqDto updateBusinessThrow = new BusinessUpdateReqDto(
                "empresa",
                "a0e8f1c1-3cec-4f60-bc1e-eb1164791745",
                "123456",
                "edwinferreira92@gmail.com",
                "Mock Prueba");

        ResponseDto responseDtoThrow = businessBusiness.update(updateBusinessThrow);
        Assertions.assertTrue(responseDtoThrow.getResultado().size() > 1);

        BusinessUpdateReqDto updateBusinessThrowName = new BusinessUpdateReqDto(
                "empresa",
                "a0e8f1c1-3cec-4f60-bc1e-eb1164791745",
                "123456789",
                "edwinferreira92@gmail.com",
                "Mo");

        ResponseDto responseDtoThrowName = businessBusiness.update(updateBusinessThrowName);
        Assertions.assertTrue(responseDtoThrowName.getResultado().size() > 1);
    }

    @Test
    void delete() {
        ResponseDto responseDto = businessBusiness.delete("3faef003-0bde-462e-b6b5-f4a05bc8896e");
        User userEntity = (User) responseDto.getData();
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", userEntity.getRowKey());
        Assertions.assertFalse(userEntity.getFechaEliminado().isEmpty());
        Assertions.assertEquals(200, responseDto.getResultado().get(0).getCodigo());

    }
}