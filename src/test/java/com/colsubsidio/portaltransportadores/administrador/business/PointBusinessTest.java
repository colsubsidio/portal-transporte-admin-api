package com.colsubsidio.portaltransportadores.administrador.business;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.PointsCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.PointsUpdateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ResponseDto;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EDateFormat;
import com.colsubsidio.portaltransportadores.administrador.commons.handler.ResponseHandler;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.Point;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.DateUtils;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.GeneralUtils;
import com.colsubsidio.portaltransportadores.administrador.integrations.dao.repository.PointsRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

class PointBusinessTest {

    PointsRepository pointsRepositoryMock = Mockito.mock(PointsRepository.class);
    ResponseHandler responseHandler = new ResponseHandler();
    GeneralUtils generalUtils = new GeneralUtils(responseHandler);
    PointBusiness pointBusiness = new PointBusiness(pointsRepositoryMock, responseHandler, generalUtils);

    @BeforeEach
    void setUp() {
        PointsCreateReqDto pointsCreateReqDto = new PointsCreateReqDto(
                "Las palmitas",
                "Calle de las palmitas",
                "10.0000, -12.0000",
                "3045761234",
                "04:00 AM - 11:00 AM",
                "Esta es la recomendacion",
                "1065638573");
        List<Point> pointsEntityList = new ArrayList<>();
        Point pointsEntity = new Point();
        pointsEntity.setNombrePunto("Las palmitas");
        pointsEntity.setDireccion("Calle de las palmitas");
        pointsEntity.setUbicacionMapa("10.0000, -12.0000");
        pointsEntity.setTelefono("3045761234");
        pointsEntity.setHorarioSalida("04:00 AM - 11:00 AM");
        pointsEntity.setRecomendaciones("Esta es la recomendacion");
        pointsEntity.setNitEmpresa("1065638573");
        pointsEntity.setPartitionKey("puntosTransporte");
        pointsEntity.setRowKey("3faef003-0bde-462e-b6b5-f4a05bc8896e");
        pointsEntity.setFechaEliminado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        pointsEntity.setFechaActualizado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        pointsEntity.setFechaCreado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        pointsEntityList.add(pointsEntity);

        Mockito.doCallRealMethod().when(pointsRepositoryMock).savePoints(pointsCreateReqDto);
        Mockito.when(pointsRepositoryMock.getPoints()).thenReturn(pointsEntityList);
        Mockito.when(pointsRepositoryMock.getPointsByRowKey("3faef003-0bde-462e-b6b5-f4a05bc8896e")).thenReturn(pointsEntity);
        Mockito.when(pointsRepositoryMock.deletePoints("3faef003-0bde-462e-b6b5-f4a05bc8896e")).thenReturn(pointsEntity);
    }

    @Test
    void obtain() {
        ResponseDto responseDto = pointBusiness.obtain();
        List<?> pointsEntityList = (List<?>) responseDto.getData();
        Point pointsEntity = (Point) pointsEntityList.get(0);
        Assertions.assertEquals(Point.class, pointsEntityList.get(0).getClass());
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", pointsEntity.getRowKey());
        Assertions.assertEquals(1, pointsEntityList.size());
        Assertions.assertEquals(200, responseDto.getResultado().get(0).getCodigo());

    }

    @Test
    void obtainByRowKey() {
        ResponseDto responseDto = pointBusiness.obtainByRowKey("3faef003-0bde-462e-b6b5-f4a05bc8896e");
        Point pointsEntity = (Point) responseDto.getData();
        Assertions.assertEquals(Point.class, pointsEntity.getClass());
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", pointsEntity.getRowKey());
        Assertions.assertEquals(200, responseDto.getResultado().get(0).getCodigo());

    }

    @Test
    void save() {
        PointsCreateReqDto pointsCreateReqDto = new PointsCreateReqDto(
                "Las palmitas",
                "Calle de las palmitas",
                "10.0000, -12.0000",
                "3045761234",
                "04:00 AM - 11:00 AM",
                "Esta es la recomendacion",
                "1065638573");
        ResponseDto responseDto = pointBusiness.save(pointsCreateReqDto);
        Assertions.assertEquals(200, responseDto.getResultado().get(0).getCodigo());

        Mockito.doCallRealMethod().when(pointsRepositoryMock).savePoints(null);
        ResponseDto responseDtoThrow = pointBusiness.save(null);
        Assertions.assertEquals(207, responseDtoThrow.getResultado().get(0).getCodigo());
    }

    @Test
    void update() {
        PointsUpdateReqDto pointsUpdateReqDto = new PointsUpdateReqDto(
                "puntosTransporte",
                "3faef003-0bde-462e-b6b5-f4a05bc8896e",
                "Las palmitas",
                "Calle de las palmitas",
                "10.0000, -12.0000",
                "3045761234",
                "04:00 AM - 11:00 AM",
                "Esta es la recomendacion",
                "1065638573"
        );

        Point pointsEntity = new Point(
                "3faef003-0bde-462e-b6b5",
                "puntosTransporte",
                "3faef003-0bde-462e-b6b5-f4a05bc8896e",
                "Las palmitas",
                "Calle de las palmitas",
                "10.0000, -12.0000",
                "3012761274",
                "04:00 AM - 11:00 AM",
                "Esta es la recomendacion",
                "1065638573",
                DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()),
                DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()),
                DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat())
        );


        Mockito.when(pointsRepositoryMock.updatePoints(pointsUpdateReqDto)).thenReturn(pointsEntity);

        ResponseDto responseDto = pointBusiness.update(pointsUpdateReqDto);
        Point points = (Point) responseDto.getData();
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", points.getRowKey());
        Assertions.assertEquals(200, responseDto.getResultado().get(0).getCodigo());
    }

    @Test
    void delete() {
        ResponseDto responseDto = pointBusiness.delete("3faef003-0bde-462e-b6b5-f4a05bc8896e");
        Point pointsEntity = (Point) responseDto.getData();
        Assertions.assertEquals("3faef003-0bde-462e-b6b5-f4a05bc8896e", pointsEntity.getRowKey());
        Assertions.assertFalse(pointsEntity.getFechaEliminado().isEmpty());
        Assertions.assertEquals(200, responseDto.getResultado().get(0).getCodigo());
    }
}