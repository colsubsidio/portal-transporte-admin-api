package com.colsubsidio.portaltransportadores.administrador.business;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.PointsCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.PointsRespDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.PointsUpdateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ResponseDto;
import com.colsubsidio.portaltransportadores.administrador.commons.handler.ResponseHandler;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.Point;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.GeneralUtils;
import com.colsubsidio.portaltransportadores.administrador.integrations.dao.repository.PointsRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.List;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class PointBusiness {

    private final @NonNull PointsRepository pointsRepository;
    private final @NonNull ResponseHandler responseHandler;
    private final @NonNull GeneralUtils generalUtils;

    public ResponseDto obtain(String nitBusiness) {
        if (StringUtils.hasLength(nitBusiness))
            return obtainByNitBusiness(nitBusiness);
        else
            return obtain();
    }

    public ResponseDto obtain() {
        try {
            List<Point> iPoints = this.pointsRepository.getPoints();
            this.responseHandler.response(HttpStatus.OK, "Puntos de transporte listados exitosamente");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(iPoints);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "getPoints", "Se produjo un fallo al momento de listar los punto de transporte");
        }
    }

    public ResponseDto obtainAll() {
        try {
            List<PointsRespDto> iPoints = this.pointsRepository.getPointsSelect();

            this.responseHandler.response(HttpStatus.OK, "Puntos de transporte para select");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(iPoints);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "getPoints", "Se produjo un fallo al momento de listar los punto de transporte");
        }
    }

    public ResponseDto obtainByRowKey(String idPoints) {
        try {
            Point points = this.pointsRepository.getPointsByRowKey(idPoints);

            this.responseHandler.response(HttpStatus.OK, "Punto de transporte listado exitosamente");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(points);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "getPointsByRowKey", "Se produjo un fallo al momento de buscar el punto de transporte por el rowKey");
        }
    }

    private ResponseDto obtainByNitBusiness(String nitBusiness) {
        try {
            List<PointsRespDto> pointsList = this.pointsRepository.getPointsByNitBusiness(nitBusiness);

            this.responseHandler.response(HttpStatus.OK, "Punto de transporte listado exitosamente");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(pointsList);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "getPointsByRowKey", "Se produjo un fallo al momento de buscar el punto de transporte por el rowKey");
        }
    }

    public ResponseDto save(PointsCreateReqDto points) {
        try {
            this.pointsRepository.savePoints(points);

            this.responseHandler.response(HttpStatus.OK, "Punto de transporte creado exitosamente");
            return this.responseHandler.response();
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "savePoints", "Se produjo un fallo al momento de crear el punto de transporte");
        }
    }

    public ResponseDto update(PointsUpdateReqDto points) {
        try {
            Point pointsResponse = this.pointsRepository.updatePoints(points);

            this.responseHandler.response(HttpStatus.OK, "Punto de transporte editado exitosamente");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(pointsResponse);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "updatePoints", "Se produjo un fallo al momento de editar el punto de transporte");
        }
    }

    public ResponseDto delete(String idPoints) {
        try {
            Point points = this.pointsRepository.deletePoints(idPoints);

            this.responseHandler.response(HttpStatus.OK, "Puntos de transporte eliminado exitosamente");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(points);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "deletePoints", "Se produjo un fallo al momento de editar el punto de transporte para registrar la fecha de eliminado");
        }
    }
}
