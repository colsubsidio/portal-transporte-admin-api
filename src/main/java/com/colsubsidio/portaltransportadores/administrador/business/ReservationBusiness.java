package com.colsubsidio.portaltransportadores.administrador.business;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.ReservationCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ReservationRespDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ReservationUpdateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ResponseDto;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EDateFormat;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.ETypeCode;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.ETypeConsumption;
import com.colsubsidio.portaltransportadores.administrador.commons.handler.ResponseHandler;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.Reservation;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.DateUtils;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.GeneralUtils;
import com.colsubsidio.portaltransportadores.administrador.integrations.dao.repository.ReservationRepository;
import com.colsubsidio.portaltransportadores.administrador.integrations.dao.services.ReservationService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.List;

@Service
@AllArgsConstructor(onConstructor = @__(@Inject))
public class ReservationBusiness {

    private final @NonNull ReservationRepository reservationRepository;
    private final @NonNull ResponseHandler responseHandler;
    private final @NonNull GeneralUtils generalUtils;
    private final @NonNull ReservationService reservationService;

    public ResponseDto obtain(String rowKey) {
        try {
            Reservation history = this.reservationRepository.getHistoryByRowKey(rowKey);

            this.responseHandler.response(HttpStatus.OK, "Historial de transporte listados exitosamente");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(history);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "obtain", "Se produjo un fallo al momento de listar el historial de transporte");
        }
    }

    public ResponseDto obtain() {
        try {
            List<Reservation> iHistory = this.reservationRepository.getHistory();

            this.responseHandler.response(HttpStatus.OK, "Historial de transporte listados exitosamente");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(iHistory);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "obtain", "Se produjo un fallo al momento de listar el historial de transporte");
        }
    }

    public ResponseDto save(List<ReservationCreateReqDto> historyList) {
        try {
            validateSave(historyList);
            this.reservationRepository.saveHistory(historyList);

            this.responseHandler.response(HttpStatus.OK, "Historial de compra de punto de transporte creado exitosamente");
            return this.responseHandler.response();
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "save", "Se produjo un fallo al momento de crear el historial de reserva");
        }
    }

    private void validateSave(List<ReservationCreateReqDto> historyList) {
        for (ReservationCreateReqDto history : historyList) {
            if (DateUtils.isBeforeCurrent(history.getFechaReserva(), EDateFormat.ISO_8601_SHORT.getFormat()))
                this.responseHandler.response(HttpStatus.NOT_ACCEPTABLE, "El valor de 'fechaReserva' no es valido ");
            if (!StringUtils.hasLength(history.getIdPunto()))
                this.responseHandler.response(HttpStatus.NOT_ACCEPTABLE, "El valor de 'idPunto' no es valido ");
            if (!StringUtils.hasLength(history.getIdReserva()))
                this.responseHandler.response(HttpStatus.NOT_ACCEPTABLE, "El valor de 'idReserva' no es valido ");
            if (!StringUtils.hasLength(history.getValor()))
                this.responseHandler.response(HttpStatus.NOT_ACCEPTABLE, "El valor de 'valor' no es valido ");
            if (!StringUtils.hasLength(history.getNumeroDocumento()))
                this.responseHandler.response(HttpStatus.NOT_ACCEPTABLE, "El valor de 'numeroDocumento' no es valido ");
        }
    }

    public ResponseDto delete(String idReservation) {
        try {
            Reservation history = this.reservationRepository.delete(idReservation);
            this.responseHandler.response(HttpStatus.OK, "Historial de reserva eliminado exitosamente");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(history);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "deleteHistory", "Se produjo un fallo al momento de editar el historial de reserva para registrar la fecha de eliminado");
        }
    }

    public ResponseDto update(ReservationUpdateReqDto reservation) {
        try {
            validateUpdate(reservation);
            List<Reservation> reservationResponse = this.reservationRepository.updateReservation(reservation);
            if (reservationResponse.isEmpty())
                this.responseHandler.response(HttpStatus.NOT_ACCEPTABLE, "No se encontraron reservas de transporte disponible para consumo con fecha actual");
            this.responseHandler.response(HttpStatus.OK, "Consumo de reserva registrado con exito");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(reservationResponse);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "update", "Se produjo un fallo al momento de registrar el consumo de la reserva");
        }
    }

    private void validateUpdate(ReservationUpdateReqDto reservation) {
        if (!EnumUtils.isValidEnum(ETypeConsumption.class, reservation.getTipoConsumo()))
            this.responseHandler.response(HttpStatus.NOT_ACCEPTABLE, "El valor de 'TipoConsumo' no es valido ");
        if (!StringUtils.hasLength(reservation.getNumeroDocumentoCliente()))
            this.responseHandler.response(HttpStatus.NOT_ACCEPTABLE, "El valor de 'NumeroDocumentoCliente' no es valido ");
        if (!StringUtils.hasLength(reservation.getNumeroDocumentoConductor()))
            this.responseHandler.response(HttpStatus.NOT_ACCEPTABLE, "El valor de 'NumeroDocumentoConductor' no es valido ");
    }

    public ResponseDto obtainTicket(String type, String code) {
        try {
            validateObtainByTypeAndCode(type, code);
            List<Reservation> reservationList = null;
            if (ETypeCode.RESERVATION.getCode().equals(type))
                reservationList = this.reservationRepository.getHistoryByIdReservationAndCurrentDate(code);
            if (ETypeCode.DOCUMENT.getCode().equals(type))
                reservationList = this.reservationRepository.getHistoryByDocumentNumberAndCurrentDate(code);
            return obtainTicket(reservationList);
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "obtainTicket", "Se produjo un fallo al momento de listar el historial de transporte");
        }
    }

    private void validateObtainByTypeAndCode(String type, String code) {
        String nameETypeCode = ETypeCode.getByCode(type);
        if (!EnumUtils.isValidEnum(ETypeCode.class, nameETypeCode))
            this.responseHandler.response(HttpStatus.NOT_ACCEPTABLE, "El valor de 'tipo' no es valido ");
        if (!StringUtils.hasLength(code))
            this.responseHandler.response(HttpStatus.NOT_ACCEPTABLE, "El valor de 'codigo' no es valido ");
    }

    private ResponseDto obtainTicket(List<Reservation> reservationList) {
        if (!reservationList.isEmpty()) {
            ReservationRespDto reservationRespDto = reservationService.obtainReservation(reservationList);
            this.responseHandler.response(HttpStatus.OK, "Reserva obtenida exitosamente ");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(reservationRespDto);
            return responseDto;
        } else {
            this.responseHandler.response(HttpStatus.NO_CONTENT, "No se encontraron reservas de transporte con los parámetros de entrada");
            return this.responseHandler.response();
        }
    }

    public ResponseDto obtainReservation(String type, String code) {
        try {
            validateObtainByTypeAndCode(type, code);
            List<Reservation> reservationList = null;
            if (ETypeCode.RESERVATION.getCode().equals(type))
                reservationList = this.reservationRepository.getHistoryByIdReservation(code);
            if (ETypeCode.DOCUMENT.getCode().equals(type))
                reservationList = this.reservationRepository.getHistoryByDocumentNumber(code);
            this.responseHandler.response(HttpStatus.OK, "Historial de transporte listados exitosamente");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(reservationList);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "obtainReservation", "Se produjo un fallo al momento de listar el historial de transporte");
        }
    }

}
