package com.colsubsidio.portaltransportadores.administrador.business;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.BusinessCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.BusinessUpdateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ResponseDto;
import com.colsubsidio.portaltransportadores.administrador.commons.handler.ResponseHandler;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.User;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.GeneralUtils;
import com.colsubsidio.portaltransportadores.administrador.integrations.dao.repository.BusinessRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class BusinessBusiness {

    private final @NonNull BusinessRepository businessRepository;
    private final @NonNull ResponseHandler responseHandler;
    private final @NonNull GeneralUtils generalUtils;

    public ResponseDto obtain() {
        try {
            List<User> iBusiness = this.businessRepository.getBusiness();

            this.responseHandler.response(HttpStatus.OK, "Empresas listadas exitosamente");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(iBusiness);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "getBusiness", "Se produjo un fallo al momento de listar las empresas");
        }
    }


    public ResponseDto obtainByRowKey(String idUser) {
        try {
            User business = this.businessRepository.getBusinessByRowKey(idUser);

            this.responseHandler.response(HttpStatus.OK, "Empresa listada exitosamente");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(business);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "getBusinessByRowKey", "Se produjo un fallo al momento de buscar la empresa por el rowKey");
        }
    }

    public ResponseDto saveBusiness(BusinessCreateReqDto user) {
        try {
            validateCreateRequest(user);
            this.businessRepository.saveBusiness(user);
            this.responseHandler.response(HttpStatus.OK, "Empresa creada exitosamente");
            return this.responseHandler.response();
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "saveBusiness", "Se produjo un fallo al momento de crear la empresa");
        }
    }

    public ResponseDto update(BusinessUpdateReqDto businessDto) {
        try {
            validateCreateRequest(businessDto);
            User business = this.businessRepository.updateBusiness(businessDto);

            this.responseHandler.response(HttpStatus.OK, "Empresa editada exitosamente");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(business);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "updateBusiness", "Se produjo un fallo al momento de editar la empresa");
        }
    }

    public ResponseDto delete(String idUser) {
        try {
            User business = this.businessRepository.deleteBusiness(idUser);

            this.responseHandler.response(HttpStatus.OK, "Empresa eliminada exitosamente");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(business);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "deleteBusiness", "Se produjo un fallo al momento de editar la empresa para registrar la fecha de eliminado");
        }
    }

    private void validateCreateRequest(String email, String nit, String name) {
        String regex = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        if (StringUtils.hasLength(email) && !matcher.matches())
            this.responseHandler.response(HttpStatus.BAD_REQUEST,
                    "El correo electrónico no cumple con el formato adecuado");

        if (StringUtils.hasLength(nit) && nit.length() <= 8)
            this.responseHandler.response(HttpStatus.BAD_REQUEST,
                    "El Número de identificación tributaria no cumple con el formato adecuado");

        if (StringUtils.hasLength(name) && name.length() < 3)
            this.responseHandler.response(HttpStatus.BAD_REQUEST,
                    "El Número de identificación tributaria no cumple con el formato adecuado");

    }

    private void validateCreateRequest(BusinessCreateReqDto businessCreateReqDto) {
        validateCreateRequest(
                businessCreateReqDto.getCorreo(),
                businessCreateReqDto.getNit(),
                businessCreateReqDto.getNombreEmpresa()
        );
    }

    private void validateCreateRequest(BusinessUpdateReqDto businessUpdateReqDto) {
        validateCreateRequest(
                businessUpdateReqDto.getCorreo(),
                businessUpdateReqDto.getNit(),
                businessUpdateReqDto.getNombreEmpresa()
        );
    }

}
