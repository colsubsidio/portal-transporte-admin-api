package com.colsubsidio.portaltransportadores.administrador.business;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.ResponseDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.UserCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.UserUpdateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.handler.ResponseHandler;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.User;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.GeneralUtils;
import com.colsubsidio.portaltransportadores.administrador.integrations.dao.repository.UserRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class UserBusiness {

    private final @NonNull UserRepository userRepository;
    private final @NonNull ResponseHandler responseHandler;
    private final @NonNull GeneralUtils generalUtils;

    public ResponseDto obtain(String nit) {
        try {
            List<User> users = this.userRepository.getUsers(nit);

            this.responseHandler.response(HttpStatus.OK, "Usuarios listados exitosamente");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(users);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "getUsers", "Se produjo un fallo al momento de listar los usuario");
        }
    }

    public ResponseDto obtainByRowKey(String idUser) {
        try {
            User user = this.userRepository.getUserByRowKey(idUser);

            this.responseHandler.response(HttpStatus.OK, "Usuarios listados exitosamente");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(user);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "getUserByRowKey", "Se produjo un fallo al momento de buscar el usuario por el rowKey");
        }
    }

    public ResponseDto save(UserCreateReqDto user) {
        try {
            this.userRepository.saveUser(user);

            this.responseHandler.response(HttpStatus.OK, "Usuario creado exitosamente");
            return this.responseHandler.response();
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "saveUser", "Se produjo un fallo al momento de crear el usuario");
        }
    }

    public ResponseDto update(UserUpdateReqDto user) {
        try {
            User userResponse = this.userRepository.updateUser(user);

            this.responseHandler.response(HttpStatus.OK, "Usuario editado exitosamente");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(userResponse);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "updateUser", "Se produjo un fallo al momento de editar el usuario");
        }
    }

    public ResponseDto delete(String idUser) {
        try {
            User user = this.userRepository.deleteUser(idUser);

            this.responseHandler.response(HttpStatus.OK, "Usuarios listados exitosamente");
            ResponseDto responseDto = this.responseHandler.response();
            responseDto.setData(user);
            return responseDto;
        } catch (Exception e) {
            return this.generalUtils.formatErrorResponse(e, "deleteUser", "Se produjo un fallo al momento de editar el usuario para registrar la fecha de eliminado");
        }
    }
}
