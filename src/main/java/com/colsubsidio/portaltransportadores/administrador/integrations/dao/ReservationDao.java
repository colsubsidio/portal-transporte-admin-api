package com.colsubsidio.portaltransportadores.administrador.integrations.dao;

import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.Reservation;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ReservationDao extends MongoRepository<Reservation, String> {
    List<Reservation> findByFechaEliminadoAndNumeroDocumentoCliente(String deleteDate, String documentNumber);
    List<Reservation> findByFechaEliminadoAndNumeroDocumentoClienteAndFechaReserva(String deleteDate, String documentNumber, String reservationDate);
    List<Reservation> findByFechaEliminadoAndNumeroDocumentoClienteAndFechaReservaAndFechaIda(String deleteDate, String documentNumber, String reservationDate, String goingDate);
    List<Reservation> findByFechaEliminadoAndNumeroDocumentoClienteAndFechaReservaAndFechaRegreso(String deleteDate, String documentNumber, String reservationDate, String returnDate);
    List<Reservation> findByFechaEliminado(String deleteDate);
    Reservation findByRowKey(String rowKey);
    List<Reservation> findByIdReserva(String idReservation);
    List<Reservation> findByIdReservaAndFechaReserva(String idReservation, String reservationDate);
}
