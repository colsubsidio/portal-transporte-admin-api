package com.colsubsidio.portaltransportadores.administrador.integrations.dao.services;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.ClientRespDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ReservationRespDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.RouteAvailabilityRespDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.TicketRespDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.reservation.PersonaDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.reservation.ReservationDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.tickets.BoletumDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.tickets.TicketDto;
import com.colsubsidio.portaltransportadores.administrador.commons.handler.ResponseHandler;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.Reservation;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.RestTemplateUtil;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.util.UriComponentsBuilder;

import javax.inject.Inject;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class ReservationService {

    private final @NonNull RestTemplateUtil restTemplateUtil;
    private final @NonNull ResponseHandler responseHandler;
    @Value("${colsubsidio.reservation.url-tickets}")
    private String urlTickets;
    @Value("${colsubsidio.reservation.url-reservation}")
    private String urlReservation;

    private List<BoletumDto> obtainTicketByIdReservationAndDocumentNumber(String idReservation, String documentNumber) {

        //UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(urlTickets.replace("{documentNumber}", documentNumber));
        UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(urlTickets);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        ResponseEntity<TicketDto> ticketDto = restTemplateUtil.sendRequest(uri, HttpMethod.GET, null, TicketDto.class, false, headers);

        return Objects.requireNonNull(ticketDto.getBody()).getBody().getObtenerEstadoBoletaUsuario().get(0)
                .getBoleta()
                .stream()
                .filter(ticket -> Integer.toString(ticket.getReserva().getCodigo()).equals(idReservation))
                .collect(Collectors.toList());
    }

    private ReservationDto obtainReservationByIdReservation(String idReservation) {
        UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(urlReservation);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        ResponseEntity<ReservationDto> ticketDto = restTemplateUtil.sendRequest(uri, HttpMethod.GET, null, ReservationDto.class, false, headers);
        return Objects.requireNonNull(ticketDto.getBody());
    }

    public ReservationRespDto obtainReservation(List<Reservation> reservationEntityList) {
        ReservationRespDto reservationRespDto = new ReservationRespDto();
        reservationRespDto.setReserva(getTicketRespDto(reservationEntityList));
        reservationRespDto.setCliente(getClientRespDto(obtainReservationByIdReservation(reservationEntityList.get(0).getIdReserva())));
        reservationRespDto.setDisponibilidadTrayecto(getRouteAvailabilityRespDto(reservationEntityList));
        return reservationRespDto;
    }

    private RouteAvailabilityRespDto getRouteAvailabilityRespDto(List<Reservation> reservationEntityList) {
        RouteAvailabilityRespDto routeAvailabilityRespDto = new RouteAvailabilityRespDto();
        routeAvailabilityRespDto.setIda(reservationEntityList.get(0).getFechaIda().isEmpty());
        routeAvailabilityRespDto.setRegreso(reservationEntityList.get(0).getFechaRegreso().isEmpty());
        return routeAvailabilityRespDto;
    }

    private TicketRespDto getTicketRespDto(List<Reservation> reservationEntityList) {
        List<BoletumDto> boletumDtoList = obtainTicketByIdReservationAndDocumentNumber(
                reservationEntityList.get(0).getIdReserva(),
                reservationEntityList.get(0).getNumeroDocumentoCliente()
        );
        if (!boletumDtoList.isEmpty()) {
            TicketRespDto ticketRespDto = new TicketRespDto();
            ticketRespDto.setCodigo(boletumDtoList.get(0).getCodigo());
            ticketRespDto.setDetalle(boletumDtoList.get(0).getProducto().getDescripcion());
            ticketRespDto.setFecha(reservationEntityList.get(0).getFechaReserva());
            return ticketRespDto;
        } else {
            this.responseHandler.response(HttpStatus.BAD_REQUEST, "No se encontraron boletas con los parámetros de entrada");
        }
        return null;
    }

    private ClientRespDto getClientRespDto(ReservationDto reservationDto) {
        if (!ObjectUtils.isEmpty(reservationDto)) {
            PersonaDto person = reservationDto.getBody().getObtenerReserva().get(0).getPersona();
            return new ClientRespDto(
                    person.getPrimerApellido(),
                    person.getPrimerNombre(),
                    person.getSegundoApellido(),
                    person.getSegundoNombre(),
                    person.getIdentificacion().getCodigo());
        } else {
            this.responseHandler.response(HttpStatus.BAD_REQUEST, "No se encontraro reserva con los parámetros de entrada");
        }
        return null;
    }
}
