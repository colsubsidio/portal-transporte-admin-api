package com.colsubsidio.portaltransportadores.administrador.integrations.dao.repository;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.PointsCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.PointsRespDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.PointsUpdateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EDateFormat;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.Point;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.DateUtils;
import com.colsubsidio.portaltransportadores.administrador.integrations.dao.PointDao;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Repository
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class PointsRepository {

    private final @NonNull PointDao pointDao;

    public List<Point> getPoints() {
        return this.pointDao.findByFechaEliminado("");
    }

    public List<PointsRespDto> getPointsSelect() {

        List<PointsRespDto> pointsDto = new ArrayList<>();

        // Loop through the results, displaying information about the entity.
        for (Point entity : getPoints()) {
            if (entity != null) {
                pointsDto.add(new PointsRespDto(entity));
            }
        }
        return pointsDto;
    }

    public Point getPointsByRowKey(String rowKey) {
        return this.pointDao.findByRowKey(rowKey);
    }

    public List<PointsRespDto> getPointsByNitBusiness(String nitBusiness) {
        List<PointsRespDto> pointsDto = new ArrayList<>();
        List<Point> pointList = this.pointDao.findByFechaEliminadoAndNitEmpresa("", nitBusiness);

        for (Point entity : pointList) {
            if (entity != null) {
                pointsDto.add(new PointsRespDto(entity));
            }
        }
        return pointsDto;
    }

    public void savePoints(PointsCreateReqDto points) {
        Point pointsEntity = new Point(points);
        pointsEntity.setFechaCreado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        this.pointDao.save(pointsEntity);
    }

    public Point updatePoints(PointsUpdateReqDto points) {
        Point pointsEntity = this.pointDao.findByRowKey(points.getRowKey());
        pointsEntity = pointsEntity.updatePointsTableStorage(pointsEntity, points);
        pointsEntity.setFechaActualizado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        return this.pointDao.save(pointsEntity);
    }

    public Point deletePoints(String rowKey) {

        Point specificPoints = this.pointDao.findByRowKey(rowKey);
        specificPoints.setFechaEliminado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        return this.pointDao.save(specificPoints);
    }

}
