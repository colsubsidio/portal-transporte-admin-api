package com.colsubsidio.portaltransportadores.administrador.integrations.dao;

import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.Point;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PointDao extends MongoRepository<Point, String> {
    List<Point> findByFechaEliminado(String deleteDate);
    List<Point> findByFechaEliminadoAndNitEmpresa(String deleteDate, String nitBusiness);
    Point findByRowKey(String rowKey);
}
