package com.colsubsidio.portaltransportadores.administrador.integrations.dao.repository;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.BusinessCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.BusinessUpdateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EDateFormat;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.User;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.DateUtils;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.ParsingUserUtils;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.ValidateUserUtils;
import com.colsubsidio.portaltransportadores.administrador.integrations.dao.UserDao;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.List;

@Repository
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class BusinessRepository {

    private final @NonNull ParsingUserUtils parsingUserUtils;
    private final @NonNull ValidateUserUtils validateUserUtils;
    private final @NonNull UserDao userDao;

    public List<User> getBusiness() {
        return this.userDao.findByFechaEliminadoAndPartitionKey("", "empresa");
    }


    public User getBusinessByRowKey(String rowKey) {
        return this.userDao.findByRowKeyAndPartitionKey(rowKey, "empresa");
    }


    public void saveBusiness(BusinessCreateReqDto business) {
        this.validateUserUtils.userExist(business.getNit());
        this.userDao.save(this.parsingUserUtils.businessCreateDtoToUser(business));
    }

    public User updateBusiness(BusinessUpdateReqDto businessDto) {
        User specificBusiness = this.userDao.findByRowKeyAndPartitionKey(businessDto.getRowKey(), businessDto.getPartitionKey());
        return this.userDao.save(this.parsingUserUtils.updateBusiness(specificBusiness, businessDto));
    }

    public User deleteBusiness(String rowKey) {
        User user = this.userDao.findByRowKeyAndPartitionKey(rowKey, "empresa");
        user.setFechaEliminado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        return this.userDao.save(user);
    }


}
