package com.colsubsidio.portaltransportadores.administrador.integrations.dao.repository;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.ReservationCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ReservationUpdateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EDateFormat;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.ETypeConsumption;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.Point;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.Reservation;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.DateUtils;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.ParsingHistoryUtils;
import com.colsubsidio.portaltransportadores.administrador.integrations.dao.ReservationDao;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Repository
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class ReservationRepository {

    private final @NonNull PointsRepository pointsRepository;
    private final @NonNull ReservationDao reservationDao;

    public List<Reservation> getHistoryByDocumentNumber(String documentNumber) {
        return this.reservationDao.findByFechaEliminadoAndNumeroDocumentoCliente("", documentNumber);
    }

    public List<Reservation> getHistory() {
        return this.reservationDao.findByFechaEliminado("");
    }

    public Reservation getHistoryByRowKey(String rowKey) {
        return this.reservationDao.findByRowKey(rowKey);
    }

    public List<Reservation> getHistoryByIdReservation(String idReservation) {
        return this.reservationDao.findByIdReserva(idReservation);
    }

    public List<Reservation> getHistoryByIdReservationAndCurrentDate(String idReservation) {
        String currentDate = DateUtils.getDateString(EDateFormat.ISO_8601_SHORT.getFormat());
        return this.reservationDao.findByIdReservaAndFechaReserva(idReservation, currentDate);
    }

    public List<Reservation> getHistoryByDocumentNumberAndCurrentDate(String documentNumber) {
        String currentDate = DateUtils.getDateString(EDateFormat.ISO_8601_SHORT.getFormat());
        return this.reservationDao.findByFechaEliminadoAndNumeroDocumentoClienteAndFechaReserva("", documentNumber, currentDate);
    }

    public void saveHistory(List<ReservationCreateReqDto> historyList) {
        for (ReservationCreateReqDto history : historyList) {
            Point point = this.pointsRepository.getPointsByRowKey(history.getIdPunto());
            this.reservationDao.save(ParsingHistoryUtils.dtoToEntity(history, point.getNitEmpresa()));
        }

    }

    public Reservation delete(String rowKey) {

        Reservation specificHistory = getHistoryByRowKey(rowKey);
        specificHistory.setFechaEliminado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        this.reservationDao.save(specificHistory);
        return specificHistory;
    }

    public List<Reservation> updateReservation(ReservationUpdateReqDto reservation) {
        String currentDate = DateUtils.getDateString(EDateFormat.ISO_8601_SHORT.getFormat());
        if (ETypeConsumption.IDA.name().equals(reservation.getTipoConsumo())) {
            List<Reservation> reservationList = this.reservationDao
                    .findByFechaEliminadoAndNumeroDocumentoClienteAndFechaReservaAndFechaIda("", reservation.getNumeroDocumentoCliente(), currentDate, "");
            return consumptionGoing(reservationList, reservation.getNumeroDocumentoConductor());
        } else {
            List<Reservation> reservationList = this.reservationDao
                    .findByFechaEliminadoAndNumeroDocumentoClienteAndFechaReservaAndFechaRegreso("", reservation.getNumeroDocumentoCliente(), currentDate, "");
            return consumptionReturn(reservationList, reservation.getNumeroDocumentoConductor());
        }
    }


    private List<Reservation> consumptionGoing(List<Reservation> reservationList, String documentNumber) {
        List<Reservation> reservation = new ArrayList<>();
        for (Reservation entity : reservationList) {
            if (entity != null) {
                entity.setFechaIda(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
                entity.setNumeroDocumentoConductor(documentNumber);
                reservation.add(this.reservationDao.save(entity));
            }
        }
        return reservation;
    }

    private List<Reservation> consumptionReturn(List<Reservation> reservationList, String documentNumber) {
        List<Reservation> reservation = new ArrayList<>();
        for (Reservation entity : reservationList) {
            if (entity != null) {
                entity.setFechaRegreso(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
                entity.setNumeroDocumentoConductor(documentNumber);
                reservation.add(this.reservationDao.save(entity));
            }
        }
        return reservation;
    }


}
