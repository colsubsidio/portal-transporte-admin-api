package com.colsubsidio.portaltransportadores.administrador.integrations.dao;

import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserDao extends MongoRepository<User, String> {
    List<User> findByFechaEliminadoAndNitEmpresa(String deleteDate, String nitBusiness);
    List<User> findByFechaEliminadoAndPartitionKey(String deleteDate, String partitionKey);
    User findByRowKeyAndPartitionKey(String rowKey, String partitionKey);
    List<User> findByFechaEliminadoAndUsuario(String deleteDate, String userName);
}
