package com.colsubsidio.portaltransportadores.administrador.integrations.dao.repository;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.UserCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.UserUpdateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EDateFormat;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.User;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.DateUtils;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.ParsingUserUtils;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.ValidateUserUtils;
import com.colsubsidio.portaltransportadores.administrador.integrations.dao.UserDao;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.List;

@Repository
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class UserRepository {

    private final @NonNull ParsingUserUtils parsingUserUtils;
    private final @NonNull ValidateUserUtils validateUserUtils;
    private final @NonNull UserDao userDao;


    public List<User> getUsers(String nit) {
        return this.userDao.findByFechaEliminadoAndNitEmpresa("", nit);
    }

    public User getUserByRowKey(String rowKey) {
        return this.userDao.findByRowKeyAndPartitionKey(rowKey, "usuario");
    }

    public void saveUser(UserCreateReqDto user) {
        this.validateUserUtils.userExist(user.getNumeroDocumento());
        this.userDao.save(this.parsingUserUtils.userCreateDtoToUser(user));
    }

    public User updateUser(UserUpdateReqDto user) {
        User userEntity = this.userDao.findByRowKeyAndPartitionKey(user.getRowKey(), user.getPartitionKey());
        userEntity = this.parsingUserUtils.updateUser(userEntity, user);
        return this.userDao.save(userEntity);
    }

    public User deleteUser(String rowKey) {
        User user = this.userDao.findByRowKeyAndPartitionKey(rowKey, "usuario");
        user.setFechaEliminado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        return this.userDao.save(user);
    }


}
