package com.colsubsidio.portaltransportadores.administrador.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class BusinessCreateReqDto implements Serializable {

    private static final long serialVersionUID = -2576374138937160581L;

    @ApiModelProperty(example = "123456789", required = true)
    private String nit;
    @ApiModelProperty(example = "pepito@transporte.com", required = true)
    private String correo;
    @ApiModelProperty(example = "Pepito Perez", required = true)
    private String nombreEmpresa;

}