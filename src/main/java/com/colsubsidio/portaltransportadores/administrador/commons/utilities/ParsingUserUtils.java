package com.colsubsidio.portaltransportadores.administrador.commons.utilities;

import com.colsubsidio.portaltransportadores.administrador.commons.constants.EmailConstant;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.*;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EDateFormat;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EDocumentType;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EProfilesUser;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EState;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.User;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.UUID;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ParsingUserUtils {

    private final @NonNull EncodeUtils encodeUtils;
    private final @NonNull NotificationsUtil notificationsUtil;

    public User userCreateDtoToUser(UserCreateReqDto userDto) {
        User user = new User();
        user.setUsuario(userDto.getNumeroDocumento());
        user.setClave(this.encodeUtils.encodePass(userDto.getNumeroDocumento()));
        user.setNumeroDocumento(userDto.getNumeroDocumento());
        user.setTipoDocumento(EDocumentType.CC.getCode());
        user.setCorreo(userDto.getCorreo());
        user.setNombreCompleto(userDto.getNombreCompleto());
        user.setPerfil(EProfilesUser.TRANSPORTADOR.getCode());
        user.setEstado(EState.ACTIVE.getCode());
        user.setNitEmpresa(userDto.getNitEmpresa());
        user.setPartitionKey("usuario");
        user.setRowKey(UUID.randomUUID().toString());
        user.setFechaEliminado("");
        user.setFechaActualizado("");
        user.setFechaCreado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        return user;
    }

    public User businessCreateDtoToUser(BusinessCreateReqDto userBusinessDto) {
        User user = new User();
        user.setUsuario(userBusinessDto.getNit());
        String password = this.generatePass();
        this.sendPassword(userBusinessDto.getCorreo(), password);
        user.setClave(this.encodeUtils.encodePass(password));
        user.setNumeroDocumento(userBusinessDto.getNit());
        user.setTipoDocumento(EDocumentType.NIT.getCode());
        user.setCorreo(userBusinessDto.getCorreo());
        user.setNombreCompleto(userBusinessDto.getNombreEmpresa());
        user.setPerfil(EProfilesUser.COMPANY.getCode());
        user.setEstado(EState.ACTIVE.getCode());
        user.setNitEmpresa("");
        user.setPartitionKey("empresa");
        user.setRowKey(UUID.randomUUID().toString());
        user.setFechaEliminado("");
        user.setFechaActualizado("");
        user.setFechaCreado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        return user;
    }

    public User updateUser(User user, UserUpdateReqDto userDto) {

        if (StringUtils.hasLength(userDto.getNumeroDocumento()))
            user.setNumeroDocumento(userDto.getNumeroDocumento());

        if (StringUtils.hasLength(userDto.getNombreCompleto()))
            user.setNombreCompleto(userDto.getNombreCompleto());

        if (StringUtils.hasLength(userDto.getNitEmpresa()))
            user.setNitEmpresa(userDto.getNitEmpresa());

        if (StringUtils.hasLength(userDto.getCorreo()))
            user.setCorreo(userDto.getCorreo());

        user.setFechaActualizado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        return user;
    }

    public User updateBusiness(User user, BusinessUpdateReqDto userBusinessDto) {

        if (StringUtils.hasLength(userBusinessDto.getNit()))
            user.setNumeroDocumento(userBusinessDto.getNit());

        if (StringUtils.hasLength(userBusinessDto.getNombreEmpresa()))
            user.setNombreCompleto(userBusinessDto.getNombreEmpresa());

        if (StringUtils.hasLength(userBusinessDto.getCorreo()))
            user.setCorreo(userBusinessDto.getCorreo());

        user.setFechaActualizado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        return user;
    }

    private void sendPassword(String email, String password) {
        EmailDto emailDto = new EmailDto();
        emailDto.setTo(email);
        emailDto.setReply("edwin.ferreira@bitsamericas.com");
        emailDto.setSubject("Contraseña de ingreso al Portal de Transporte de Colsubsidio");
        emailDto.setText(EmailConstant.BODY_EMAIL.replace("{password}", password));
        this.notificationsUtil.email(emailDto);
    }

    private String generatePass() {
        int length = 7;
        return RandomStringUtils.random(length, true, true);
    }
}
