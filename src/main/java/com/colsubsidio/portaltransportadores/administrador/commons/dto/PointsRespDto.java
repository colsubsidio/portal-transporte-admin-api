package com.colsubsidio.portaltransportadores.administrador.commons.dto;

import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.Point;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PointsRespDto implements Serializable {

    private static final long serialVersionUID = 2095944177927209761L;
    private String rowKey;
    private String nombrePunto;
    private String direccion;
    private String ubicacionMapa;
    private String telefono;
    private String horarioSalida;
    private String recomendaciones;
    private String nitEmpresa;
    private String fechaCreado;
    private String fechaActualizado;
    private String fechaEliminado;

    public PointsRespDto(Point points) {
        this.nombrePunto = points.getNombrePunto();
        this.direccion = points.getDireccion();
        this.ubicacionMapa = points.getUbicacionMapa();
        this.telefono = points.getTelefono();
        this.horarioSalida = points.getHorarioSalida();
        this.recomendaciones = points.getRecomendaciones();
        this.nitEmpresa = points.getNitEmpresa();
        this.fechaCreado = points.getFechaCreado();
        this.fechaActualizado = points.getFechaActualizado();
        this.fechaEliminado = points.getFechaEliminado();
        this.rowKey = points.getRowKey();
    }

}
