package com.colsubsidio.portaltransportadores.administrador.commons.utilities;

import com.colsubsidio.portaltransportadores.administrador.commons.handler.ResponseHandler;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.User;
import com.colsubsidio.portaltransportadores.administrador.integrations.dao.UserDao;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class ValidateUserUtils {

    private final @NonNull ResponseHandler responseHandler;
    private final @NonNull UserDao userDao;


    public void userExist(String user) {
        List<User> userList = this.userDao.findByFechaEliminadoAndUsuario("", user);


        for (User entity : userList) {
            if (entity != null) {
                this.responseHandler.response(
                        HttpStatus.BAD_REQUEST,
                        "Ya existe un usuario registrado con el número de identificación: ".concat(user));
            }
        }
    }

}
