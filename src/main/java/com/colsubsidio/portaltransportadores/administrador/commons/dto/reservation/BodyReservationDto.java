package com.colsubsidio.portaltransportadores.administrador.commons.dto.reservation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class BodyReservationDto {
    private List<ResultadoDto> resultado = null;
    private List<ObtenerReservaDto> obtenerReserva = null;
}
