package com.colsubsidio.portaltransportadores.administrador.commons.dto.tickets;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CentroServicioDto {
    private String codigo;
    private String nombre;
}
