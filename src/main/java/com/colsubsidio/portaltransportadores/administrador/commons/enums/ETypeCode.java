package com.colsubsidio.portaltransportadores.administrador.commons.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ETypeCode {
    DOCUMENT("2"),
    RESERVATION("1");

    private String code;

    public static String getByCode(String code) {
        for (ETypeCode e : values()) {
            if (e.code.equals(code))
                return e.name();
        }
        return "";
    }
}
