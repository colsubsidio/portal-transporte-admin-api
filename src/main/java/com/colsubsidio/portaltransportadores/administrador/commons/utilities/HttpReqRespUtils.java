package com.colsubsidio.portaltransportadores.administrador.commons.utilities;

import com.colsubsidio.portaltransportadores.administrador.commons.exceptions.AuthenticationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Component
public class HttpReqRespUtils {


    private static final String LOCALHOST = "0:0:0:0:0:0:0:1";
    private static final String LOCALHOST_NAME = "LOCALHOST";
    private static final String[] IP_HEADER_CANDIDATES = {
            "X-Forwarded-For",
            "Proxy-Client-IP",
            "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "REMOTE_ADDR"
    };
    private static String OBTAIN_IP_STATIC;

    private static String getClientIpAddressIfServletRequestExist() {

        if (RequestContextHolder.getRequestAttributes() == null)
            return LOCALHOST_NAME;

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        for (String header : IP_HEADER_CANDIDATES) {
            String ipList = request.getHeader(header);
            if (ipList != null && ipList.length() != 0 && !"unknown".equalsIgnoreCase(ipList)) {
                return ipList.split(",")[0];
            }
        }

        if (LOCALHOST.equals(request.getRemoteAddr()))
            return LOCALHOST_NAME;

        return request.getRemoteAddr();
    }

    public static void validateIp() throws AuthenticationException {
        String ipAddressAndPort = getClientIpAddressIfServletRequestExist();
        String ipAddress = ipAddressAndPort.split(":")[0];
        for (String ip : OBTAIN_IP_STATIC.split(",")) {
            if (ip.equals(ipAddress))
                return;
        }
        throw new AuthenticationException(
                "Esta dirección IP no se encuentra autorizada para el acceso.");
    }

    @Value("${colsubsidio.ipAddress}")
    public void setStaticIpAddress(String ipAddressString) {
        HttpReqRespUtils.OBTAIN_IP_STATIC = ipAddressString;
    }

}
