package com.colsubsidio.portaltransportadores.administrador.commons.models.documents;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "reservation")
public class Reservation implements Serializable {

    private static final long serialVersionUID = -1009250121582849236L;
    @Id
    private String id;

    private String partitionKey;
    private String rowKey;
    private String idReserva;
    private String valor;
    private String numeroDocumentoCliente;
    private String numeroDocumentoConductor;
    private String fechaReserva;
    private String fechaCreado;
    private String fechaIda;
    private String fechaRegreso;
    private String fechaEliminado;
    private String nitEmpresa;
}
