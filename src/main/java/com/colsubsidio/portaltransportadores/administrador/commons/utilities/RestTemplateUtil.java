package com.colsubsidio.portaltransportadores.administrador.commons.utilities;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.inject.Inject;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class RestTemplateUtil {

    private final @NonNull RestTemplate restTemplate;

    public <T> ResponseEntity<T> sendRequest(UriComponentsBuilder uri, HttpMethod method, Object body,
                                             Class<T> classOfT, boolean apigeeToken, HttpHeaders headerConfig) throws RestClientException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        if (headerConfig != null)
            headers = headerConfig;

        HttpEntity<Object> entity = (body != null) ? new HttpEntity<>(body, headers) : new HttpEntity<>(headers);
        return restTemplate.exchange(uri.toUriString(), method, entity, classOfT);
    }
}

