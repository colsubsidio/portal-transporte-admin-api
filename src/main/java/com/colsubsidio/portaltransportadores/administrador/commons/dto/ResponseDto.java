package com.colsubsidio.portaltransportadores.administrador.commons.dto;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ResponseDto implements Serializable {

    private static final long serialVersionUID = -1412512895197057870L;
    private List<ResultDto> resultado;
    private Object data;

}
