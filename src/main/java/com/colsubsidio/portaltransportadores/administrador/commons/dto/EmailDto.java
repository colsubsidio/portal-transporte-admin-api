package com.colsubsidio.portaltransportadores.administrador.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EmailDto implements Serializable {
    private static final long serialVersionUID = 5194556864990008245L;
    private String to;
    private String subject;
    private String reply;
    private String text;
}
