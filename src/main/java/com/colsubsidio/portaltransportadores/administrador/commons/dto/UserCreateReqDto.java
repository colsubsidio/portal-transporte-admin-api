package com.colsubsidio.portaltransportadores.administrador.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserCreateReqDto implements Serializable {

    private static final long serialVersionUID = 1229894976786955643L;

    @ApiModelProperty(example = "123456789", required = true)
    private String numeroDocumento;
    @ApiModelProperty(example = "pepito@transporte.com")
    private String correo;
    @ApiModelProperty(example = "Pepito Perez", required = true)
    private String nombreCompleto;
    @ApiModelProperty(example = "123456789", required = true)
    private String nitEmpresa;


}
