package com.colsubsidio.portaltransportadores.administrador.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PointsCreateReqDto implements Serializable {

    private static final long serialVersionUID = 2307271835488593936L;

    @ApiModelProperty(example = "Las Palmitas", required = true)
    private String nombrePunto;
    @ApiModelProperty(example = "Calle 198 Norte", required = true)
    private String direccion;
    @ApiModelProperty(example = "10.00000, -40.00000", required = true)
    private String ubicacionMapa;
    @ApiModelProperty(example = "301287993", required = true)
    private String telefono;
    @ApiModelProperty(example = "08:00 a.m. - 09:00 a.m", required = true)
    private String horarioSalida;
    @ApiModelProperty(example = "Prueba texto", required = true)
    private String recomendaciones;
    @ApiModelProperty(example = "123456789", required = true)
    private String nitEmpresa;
}
