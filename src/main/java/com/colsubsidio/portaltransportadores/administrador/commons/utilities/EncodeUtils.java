package com.colsubsidio.portaltransportadores.administrador.commons.utilities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class EncodeUtils {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public EncodeUtils(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public String encodePass(String value) {
        return this.passwordEncoder.encode(value);
    }

    public boolean comparePass(String passText, String passEncode) {
        return this.passwordEncoder.matches(passText, passEncode);
    }
}
