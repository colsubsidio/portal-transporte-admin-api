package com.colsubsidio.portaltransportadores.administrador.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TicketRespDto implements Serializable {
    private static final long serialVersionUID = 2167275576767388637L;
    private String codigo;
    private String detalle;
    private String fecha;
}
