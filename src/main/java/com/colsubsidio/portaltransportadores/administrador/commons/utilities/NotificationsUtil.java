package com.colsubsidio.portaltransportadores.administrador.commons.utilities;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.EmailDto;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import javax.inject.Inject;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class NotificationsUtil {

    private final @NonNull RestTemplateUtil restTemplateUtil;

    @Value("${colsubsidio.notifications.url-email}")
    private String urlEmail;

    public void email(EmailDto emailDto) {
        UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(urlEmail);

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "bm9yZXBseQ==");
        headers.set("Content-Type", "application/json");

        restTemplateUtil.sendRequest(uri, HttpMethod.POST, emailDto, String.class, false, headers);
    }

}
