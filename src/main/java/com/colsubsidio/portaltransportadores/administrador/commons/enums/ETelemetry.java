package com.colsubsidio.portaltransportadores.administrador.commons.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ETelemetry {
    TAG_TELEMETRY("telemetry"),
    ERROR("error"),
    INFO("info");

    private final String detalle;
}
