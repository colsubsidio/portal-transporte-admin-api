package com.colsubsidio.portaltransportadores.administrador.commons.exceptions;

public class AuthenticationException extends Exception {


    private static final long serialVersionUID = 3638953301489239090L;

    public AuthenticationException(String message) {
        super(message);
    }

    public AuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }

}
