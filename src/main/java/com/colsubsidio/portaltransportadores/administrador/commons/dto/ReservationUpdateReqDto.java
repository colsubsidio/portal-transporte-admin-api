package com.colsubsidio.portaltransportadores.administrador.commons.dto;

import com.colsubsidio.portaltransportadores.administrador.commons.enums.ETypeConsumption;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
public class ReservationUpdateReqDto implements Serializable {

    private static final long serialVersionUID = -5835680431969817396L;

    @ApiModelProperty(example = "1065678900", required = true)
    private String numeroDocumentoCliente;

    @ApiModelProperty(example = "1065678901", required = true)
    private String numeroDocumentoConductor;

    @ApiModelProperty(example = "IDA", required = true)
    private String tipoConsumo;
}
