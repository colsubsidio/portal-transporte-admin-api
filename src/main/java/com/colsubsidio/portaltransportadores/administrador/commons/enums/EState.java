package com.colsubsidio.portaltransportadores.administrador.commons.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EState {
    ACTIVE("1", "Activo"),
    INACTIVE("2", "Inactivo");

    private String code;
    private String detail;
}
