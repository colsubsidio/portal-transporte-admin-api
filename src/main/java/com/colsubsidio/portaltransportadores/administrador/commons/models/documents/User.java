package com.colsubsidio.portaltransportadores.administrador.commons.models.documents;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "user")
public class User implements Serializable {

    private static final long serialVersionUID = -4981091768592800353L;

    @Id
    private String id;

    private String partitionKey;
    private String rowKey;
    private String usuario;
    private String numeroDocumento;
    private String tipoDocumento;
    private String clave;
    private String correo;
    private String nombreCompleto;
    private String estado;
    private String perfil;
    private String nitEmpresa;
    private String fechaCreado;
    private String fechaActualizado;
    private String fechaEliminado;
}
