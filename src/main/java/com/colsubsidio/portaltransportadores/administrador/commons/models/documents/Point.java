package com.colsubsidio.portaltransportadores.administrador.commons.models.documents;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.PointsCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.PointsUpdateReqDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "point")
public class Point implements Serializable {

    private static final long serialVersionUID = -7243350354310779684L;
    @Id
    private String id;

    private String partitionKey;
    private String rowKey;
    private String nombrePunto;
    private String direccion;
    private String ubicacionMapa;
    private String telefono;
    private String horarioSalida;
    private String recomendaciones;
    private String nitEmpresa;
    private String fechaCreado;
    private String fechaActualizado;
    private String fechaEliminado;

    public Point(PointsCreateReqDto points) {
        this.nombrePunto = points.getNombrePunto();
        this.direccion = points.getDireccion();
        this.ubicacionMapa = points.getUbicacionMapa();
        this.telefono = points.getTelefono();
        this.horarioSalida = points.getHorarioSalida();
        this.recomendaciones = points.getRecomendaciones();
        this.nitEmpresa = points.getNitEmpresa();
        this.partitionKey = "puntosTransporte";
        this.rowKey = UUID.randomUUID().toString();
        this.fechaEliminado = "";
    }

    public Point updatePointsTableStorage(Point points, PointsUpdateReqDto pointsDto) {
        if (StringUtils.hasLength(pointsDto.getNombrePunto()))
            points.setNombrePunto(pointsDto.getNombrePunto());

        if (StringUtils.hasLength(pointsDto.getDireccion()))
            points.setDireccion(pointsDto.getDireccion());

        if (StringUtils.hasLength(pointsDto.getUbicacionMapa()))
            points.setUbicacionMapa(pointsDto.getUbicacionMapa());

        if (StringUtils.hasLength(pointsDto.getTelefono()))
            points.setTelefono(pointsDto.getTelefono());

        if (StringUtils.hasLength(pointsDto.getHorarioSalida()))
            points.setHorarioSalida(pointsDto.getHorarioSalida());

        if (StringUtils.hasLength(pointsDto.getRecomendaciones()))
            points.setRecomendaciones(pointsDto.getRecomendaciones());

        if (StringUtils.hasLength(pointsDto.getNitEmpresa()))
            points.setNitEmpresa(pointsDto.getNitEmpresa());

        return points;
    }
}
