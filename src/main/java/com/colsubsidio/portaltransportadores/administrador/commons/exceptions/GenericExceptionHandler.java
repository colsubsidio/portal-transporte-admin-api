package com.colsubsidio.portaltransportadores.administrador.commons.exceptions;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.ResponseDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ResultDto;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class GenericExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String CONTENT_TYPE_KEY = "Content-Type";

    private static ResponseDto buildDefaultExceptionBody(int code, String detail) {
        ResultDto resultDto = ResultDto.builder().codigo(code).descripcion(detail).build();
        ResponseDto responseDto = new ResponseDto();
        List<ResultDto> resultDtoList = new ArrayList<>();
        resultDtoList.add(resultDto);
        responseDto.setResultado(resultDtoList);
        return responseDto;
    }

    private static ResponseDto buildDefaultExceptionBody(int code, String detail, ResponseDto responseDto) {
        ResultDto resultDto = ResultDto.builder().codigo(code).descripcion(detail).build();
        List<ResultDto> resultDtoList = responseDto.getResultado();
        resultDtoList.add(resultDto);
        responseDto.setResultado(resultDtoList);
        return responseDto;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleAnyException(Exception ex, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(CONTENT_TYPE_KEY, MediaType.APPLICATION_JSON_VALUE);
        ResponseDto response = buildDefaultExceptionBody(HttpStatus.MULTI_STATUS.value(), ex.getLocalizedMessage());
        response = buildDefaultExceptionBody(
                HttpStatus.MULTI_STATUS.value(),
                "¡Uups! algo salió mal. Parece que hay un problema con el recurso que está buscando y no se puede mostrar.",
                response);
        return new ResponseEntity<>(response, headers, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<Object> handleBusinessException(BusinessException ex, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(CONTENT_TYPE_KEY, MediaType.APPLICATION_JSON_VALUE);
        ResponseDto response = buildDefaultExceptionBody(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage());
        return new ResponseEntity<>(response, headers, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<Object> handleAuthenticationException(AuthenticationException ex, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(CONTENT_TYPE_KEY, MediaType.APPLICATION_JSON_VALUE);
        ResponseDto response = buildDefaultExceptionBody(HttpStatus.UNAUTHORIZED.value(), ex.getLocalizedMessage());
        return new ResponseEntity<>(response, headers, HttpStatus.UNAUTHORIZED);
    }

    @Override
    public ResponseEntity<Object> handleHttpMediaTypeNotSupported(
            HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status,
            WebRequest request) {
        headers.add(CONTENT_TYPE_KEY, MediaType.APPLICATION_JSON_VALUE);
        ResponseDto response = buildDefaultExceptionBody(status.value(), ex.getLocalizedMessage());
        response = buildDefaultExceptionBody(
                status.value(),
                "Solo consumimos application/json;charset=utf-8 media type.",
                response);
        return new ResponseEntity<>(response, headers, status);
    }

    @Override
    public ResponseEntity<Object> handleHttpMediaTypeNotAcceptable(
            HttpMediaTypeNotAcceptableException ex, HttpHeaders headers, HttpStatus status,
            WebRequest request) {
        headers.add(CONTENT_TYPE_KEY, MediaType.APPLICATION_JSON_VALUE);
        ResponseDto response = buildDefaultExceptionBody(status.value(), ex.getLocalizedMessage());
        response = buildDefaultExceptionBody(
                status.value(),
                "Solo producimos application/json;charset=utf-8 media type.",
                response);
        return new ResponseEntity<>(response, headers, status);
    }

    @Override
    public ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status,
            WebRequest request) {
        headers.add(CONTENT_TYPE_KEY, MediaType.APPLICATION_JSON_VALUE);
        ResponseDto response = buildDefaultExceptionBody(status.value(), ex.getLocalizedMessage());
        response = buildDefaultExceptionBody(
                status.value(),
                "Uups! ¿Estás perdido? Compruebe el método que está intentando utilizar.",
                response);
        return new ResponseEntity<>(response, headers, status);
    }

    @Override
    public ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex,
                                                                HttpHeaders headers, HttpStatus status, WebRequest request) {
        headers.add(CONTENT_TYPE_KEY, MediaType.APPLICATION_JSON_VALUE);
        ResponseDto response = buildDefaultExceptionBody(status.value(), ex.getLocalizedMessage());
        response = buildDefaultExceptionBody(
                status.value(),
                "Uups! ¿Estás perdido? Verifique el recurso al que está intentando acceder.",
                response);
        return new ResponseEntity<>(response, headers, status);
    }

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                               HttpHeaders headers, HttpStatus status, WebRequest request) {
        headers.add(CONTENT_TYPE_KEY, MediaType.APPLICATION_JSON_VALUE);
        ResponseDto response = buildDefaultExceptionBody(status.value(), ex.getLocalizedMessage());
        response = buildDefaultExceptionBody(
                status.value(),
                "Faltan uno o más argumentos. Por favor revise la solicitud.",
                response);
        return new ResponseEntity<>(response, headers, status);
    }

    @Override
    public ResponseEntity<Object> handleAsyncRequestTimeoutException(AsyncRequestTimeoutException ex,
                                                                     HttpHeaders headers, HttpStatus status, WebRequest webRequest) {
        headers.add(CONTENT_TYPE_KEY, MediaType.APPLICATION_JSON_VALUE);
        ResponseDto response = buildDefaultExceptionBody(status.value(), ex.getLocalizedMessage());
        response = buildDefaultExceptionBody(
                status.value(),
                "Se alcanzó el tiempo de espera. Inténtelo de nuevo en un momento.",
                response);
        return new ResponseEntity<>(response, headers, status);
    }
}
