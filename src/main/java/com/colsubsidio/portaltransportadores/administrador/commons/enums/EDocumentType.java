package com.colsubsidio.portaltransportadores.administrador.commons.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EDocumentType {
    CC("1", "Cedula de ciudadanía"),
    NIT("2", "Número de identificación tributaria");

    private String code;
    private String detail;
}
