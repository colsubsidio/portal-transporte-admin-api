package com.colsubsidio.portaltransportadores.administrador.commons.utilities;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.ResponseDto;
import com.colsubsidio.portaltransportadores.administrador.commons.handler.ResponseHandler;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class GeneralUtils {

    private final @NonNull ResponseHandler responseHandler;

    public ResponseDto formatErrorResponse(Exception e, String method, String message) {
        this.responseHandler.response(HttpStatus.MULTI_STATUS,
                message);
        return this.responseHandler.response();
    }
}
