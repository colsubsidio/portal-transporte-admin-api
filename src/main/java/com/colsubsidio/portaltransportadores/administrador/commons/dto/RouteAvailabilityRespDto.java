package com.colsubsidio.portaltransportadores.administrador.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RouteAvailabilityRespDto implements Serializable {
    private static final long serialVersionUID = 6369651735311379128L;
    private Boolean ida;
    private Boolean regreso;
}
