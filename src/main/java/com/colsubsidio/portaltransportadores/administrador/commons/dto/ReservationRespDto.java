package com.colsubsidio.portaltransportadores.administrador.commons.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ReservationRespDto implements Serializable {
    private static final long serialVersionUID = 2348473877271222791L;
    public TicketRespDto reserva;
    public ClientRespDto cliente;
    public RouteAvailabilityRespDto disponibilidadTrayecto;
}
