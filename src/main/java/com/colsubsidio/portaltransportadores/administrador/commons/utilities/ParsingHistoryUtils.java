package com.colsubsidio.portaltransportadores.administrador.commons.utilities;

import com.colsubsidio.portaltransportadores.administrador.commons.dto.ReservationCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.enums.EDateFormat;
import com.colsubsidio.portaltransportadores.administrador.commons.models.documents.Reservation;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class ParsingHistoryUtils {

    public static Reservation dtoToEntity(ReservationCreateReqDto historyDto, String nitEmpresa) {

        Reservation reservationEntity = new Reservation();
        reservationEntity.setPartitionKey(historyDto.getIdPunto());
        reservationEntity.setRowKey(UUID.randomUUID().toString());
        reservationEntity.setIdReserva(historyDto.getIdReserva());
        reservationEntity.setNumeroDocumentoCliente(historyDto.getNumeroDocumento());
        reservationEntity.setNumeroDocumentoConductor("");
        reservationEntity.setValor(historyDto.getValor());
        reservationEntity.setFechaReserva(historyDto.getFechaReserva());
        reservationEntity.setFechaEliminado("");
        reservationEntity.setFechaIda("");
        reservationEntity.setFechaRegreso("");
        reservationEntity.setFechaCreado(DateUtils.getDateString(EDateFormat.ISO_8601_MILI_LONG.getFormat()));
        reservationEntity.setNitEmpresa(nitEmpresa);
        return reservationEntity;
    }
}
