package com.colsubsidio.portaltransportadores.administrador.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class BusinessUpdateReqDto implements Serializable {

    private static final long serialVersionUID = -1158495634822321109L;

    @ApiModelProperty(example = "empresa", required = true)
    private String partitionKey;
    @ApiModelProperty(example = "8fc90f01-cfe3-4f01-8da2-c36aa3c930fc", required = true)
    private String rowKey;
    @ApiModelProperty(example = "123456789")
    private String nit;
    @ApiModelProperty(example = "pepito@transporte.com")
    private String correo;
    @ApiModelProperty(example = "Pepito Perez")
    private String nombreEmpresa;
}
