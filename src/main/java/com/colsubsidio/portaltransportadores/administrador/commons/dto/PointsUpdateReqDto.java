package com.colsubsidio.portaltransportadores.administrador.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PointsUpdateReqDto implements Serializable {

    private static final long serialVersionUID = -5989504057000040717L;

    @ApiModelProperty(example = "puntosTransporte", required = true)
    private String partitionKey;
    @ApiModelProperty(example = "8fc90f01-cfe3-4f01-8da2-c36aa3c930fc", required = true)
    private String rowKey;
    @ApiModelProperty(example = "Las Palmitas")
    private String nombrePunto;
    @ApiModelProperty(example = "Calle 198 Norte")
    private String direccion;
    @ApiModelProperty(example = "10.00000, -40.00000")
    private String ubicacionMapa;
    @ApiModelProperty(example = "301287993")
    private String telefono;
    @ApiModelProperty(example = "08:00 a.m. - 09:00 a.m")
    private String horarioSalida;
    @ApiModelProperty(example = "Prueba texto")
    private String recomendaciones;
    @ApiModelProperty(example = "123456789")
    private String nitEmpresa;
}
