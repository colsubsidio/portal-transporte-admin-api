package com.colsubsidio.portaltransportadores.administrador.commons.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public class ReservationCreateReqDto implements Serializable {

    private static final long serialVersionUID = 281417422288271959L;

    @ApiModelProperty(example = "8fc90f01-cfe3-4f01-8da2-c36aa3c930fc", required = true)
    private String idPunto;
    @ApiModelProperty(example = "345", required = true)
    private String idReserva;
    @ApiModelProperty(example = "12000.80", required = true)
    private String valor;
    @ApiModelProperty(example = "123456789", required = true)
    private String numeroDocumento;
    @ApiModelProperty(example = "2021-04-10", required = true)
    private String fechaReserva;
}
