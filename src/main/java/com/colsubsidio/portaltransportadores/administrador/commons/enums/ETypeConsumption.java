package com.colsubsidio.portaltransportadores.administrador.commons.enums;

public enum ETypeConsumption {
    IDA,
    REGRESO
}
