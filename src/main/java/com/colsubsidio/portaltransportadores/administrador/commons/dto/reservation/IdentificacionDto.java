package com.colsubsidio.portaltransportadores.administrador.commons.dto.reservation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class IdentificacionDto {

    private String codigo;
    private String descripcion;
    private int tipo;
}
