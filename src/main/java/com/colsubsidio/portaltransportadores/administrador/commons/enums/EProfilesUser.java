package com.colsubsidio.portaltransportadores.administrador.commons.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EProfilesUser {

    ADMIN("1", "Administrador", "ROLE_ADMIN"),
    TRANSPORTADOR("2", "Tranportador", "ROLE_TRANSPORT"),
    COMPANY("3", "Empresa", "ROLE_COMPANY"),
    NA("0", "NA", "NA");

    private final String code;
    private final String description;
    private final String role;

    public static EProfilesUser getByCode(String code) {
        for (EProfilesUser e : values()) {
            if (e.code.equals(code))
                return e;
        }
        return NA;
    }

    public static EProfilesUser getByDescription(String description) {
        for (EProfilesUser e : values()) {
            if (e.description.equals(description))
                return e;
        }
        return NA;
    }

    public static String getByCode(EProfilesUser stateReserveTypeEnum) {
        for (EProfilesUser e : values()) {
            if (e == stateReserveTypeEnum)
                return e.getCode();
        }
        return NA.getCode();
    }
}
