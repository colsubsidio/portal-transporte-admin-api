package com.colsubsidio.portaltransportadores.administrador.commons.dto.reservation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ObtenerReservaDto {
    private String canal;
    private int cantidad;
    private CiudadDto ciudad;
    private String comentarios;
    private EspacioDto espacio;
    private String fecha;
    private FranjaDto franja;
    private int id;
    private int idCorporativa;
    private PersonaDto persona;
    private SitioDto sitio;
}
