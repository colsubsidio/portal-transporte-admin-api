package com.colsubsidio.portaltransportadores.administrador.api;

import com.colsubsidio.portaltransportadores.administrador.business.UserBusiness;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ResponseDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.UserCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.UserUpdateReqDto;
import io.swagger.annotations.ApiOperation;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("usuarios")
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class UserController {

    private final @NonNull UserBusiness userBusiness;

    @ApiOperation(value = "Listar transportadores por NIT de empresa")
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> obtainAll(@RequestParam(name = "nit") String nit) {
        return new ResponseEntity<>(this.userBusiness.obtain(nit), HttpStatus.OK);
    }

    @ApiOperation(value = "Buscar transportador")
    @GetMapping(value = "/{rowKey}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> obtainByRowKey(@PathVariable(name = "rowKey") String idUser) {
        return new ResponseEntity<>(this.userBusiness.obtainByRowKey(idUser), HttpStatus.OK);
    }

    @ApiOperation(value = "Crear transportador")
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> create(@RequestBody UserCreateReqDto user) {
        return new ResponseEntity<>(this.userBusiness.save(user), HttpStatus.OK);
    }

    @ApiOperation(value = "Actualizar transportador")
    @PutMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> update(@RequestBody UserUpdateReqDto user) {
        return new ResponseEntity<>(this.userBusiness.update(user), HttpStatus.OK);
    }

    @ApiOperation(value = "Inhabilitar transportador")
    @DeleteMapping(value = "/{rowKey}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> delete(@PathVariable(name = "rowKey") String idUser) {
        return new ResponseEntity<>(this.userBusiness.delete(idUser), HttpStatus.OK);
    }
}
