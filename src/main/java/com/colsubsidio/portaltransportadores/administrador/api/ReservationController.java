package com.colsubsidio.portaltransportadores.administrador.api;

import com.colsubsidio.portaltransportadores.administrador.business.ReservationBusiness;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ReservationCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ReservationUpdateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ResponseDto;
import io.swagger.annotations.ApiOperation;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping("reservas")
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class ReservationController {

    private final @NonNull ReservationBusiness reservationBusiness;

    @ApiOperation(value = "Listado de reservas de transporte")
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> obtainAll() {
        return new ResponseEntity<>(this.reservationBusiness.obtain(), HttpStatus.OK);
    }

    @ApiOperation(value = "Buscar reserva de transporte por rowKey")
    @GetMapping(value = "{rowKey}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> obtain(
            @PathVariable(name = "rowKey") String rowKey) {
        return new ResponseEntity<>(this.reservationBusiness.obtain(rowKey), HttpStatus.OK);
    }

    @ApiOperation(value = "Información de boleta y transporte por tipo y codigo")
    @GetMapping(value = "boletas", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> obtainTicket(
            @RequestParam(name = "tipo") String type,
            @RequestParam(name = "codigo") String code) {
        return new ResponseEntity<>(this.reservationBusiness.obtainTicket(type, code), HttpStatus.OK);
    }

    @ApiOperation(value = "Listado de reserva de transporte por tipo y codigo")
    @GetMapping(value = "transporte", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> obtainReservation(
            @RequestParam(name = "tipo") String type,
            @RequestParam(name = "codigo") String code) {
        return new ResponseEntity<>(this.reservationBusiness.obtainReservation(type, code), HttpStatus.OK);
    }

    @ApiOperation(value = "Crear reserva de transporte")
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> create(@RequestBody List<ReservationCreateReqDto> reservationDtoList) {
        return new ResponseEntity<>(this.reservationBusiness.save(reservationDtoList), HttpStatus.OK);
    }

    @ApiOperation(value = "Registrar consumos de reservas de transporte")
    @PutMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> update(@RequestBody ReservationUpdateReqDto reservationUpdateReqDtot) {
        return new ResponseEntity<>(this.reservationBusiness.update(reservationUpdateReqDtot), HttpStatus.OK);
    }

    @ApiOperation(value = "Inhabilitar reserva de transporte")
    @DeleteMapping(value = "{rowKey}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> delete(@PathVariable(name = "rowKey") String idReservation) {
        return new ResponseEntity<>(this.reservationBusiness.delete(idReservation), HttpStatus.OK);
    }
}
