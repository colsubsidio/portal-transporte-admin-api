package com.colsubsidio.portaltransportadores.administrador.api;

import com.colsubsidio.portaltransportadores.administrador.business.PointBusiness;
import com.colsubsidio.portaltransportadores.administrador.business.ReservationBusiness;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ReservationCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ResponseDto;
import com.colsubsidio.portaltransportadores.administrador.commons.exceptions.AuthenticationException;
import com.colsubsidio.portaltransportadores.administrador.commons.utilities.HttpReqRespUtils;
import io.swagger.annotations.ApiOperation;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping("apigee")
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class ApigeeController {

    private final @NonNull PointBusiness pointBusiness;
    private final @NonNull ReservationBusiness reservationBusiness;

    @ApiOperation(value = "Listado puntos de transporte")
    @GetMapping(value = "/puntos", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> obtainApigee( ) throws AuthenticationException {
        HttpReqRespUtils.validateIp();
        return new ResponseEntity<>(this.pointBusiness.obtain(), HttpStatus.OK);
    }

    @ApiOperation(value = "Crear reserva de transporte")
    @PostMapping(value = "/reservas", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> createApigee(@RequestBody List<ReservationCreateReqDto> reservationDtoList) throws AuthenticationException {
        HttpReqRespUtils.validateIp();
        return new ResponseEntity<>(this.reservationBusiness.save(reservationDtoList), HttpStatus.OK);
    }

    @ApiOperation(value = "Buscar reserva de transporte por rowKey")
    @GetMapping(value = "/reservas/{rowKey}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> obtainApigee(
            @PathVariable(name = "rowKey") String rowKey) throws AuthenticationException {
        HttpReqRespUtils.validateIp();
        return new ResponseEntity<>(this.reservationBusiness.obtain(rowKey), HttpStatus.OK);
    }

    @ApiOperation(value = "Inhabilitar reserva de transporte")
    @DeleteMapping(value = "/reservas/{rowKey}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> deleteApigee(@PathVariable(name = "rowKey") String idReservation) throws AuthenticationException {
        HttpReqRespUtils.validateIp();
        return new ResponseEntity<>(this.reservationBusiness.delete(idReservation), HttpStatus.OK);
    }
}
