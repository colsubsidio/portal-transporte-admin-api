package com.colsubsidio.portaltransportadores.administrador.api;

import com.colsubsidio.portaltransportadores.administrador.business.PointBusiness;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.PointsCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.PointsUpdateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ResponseDto;
import io.swagger.annotations.ApiOperation;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("puntos")
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class PointController {

    private final @NonNull PointBusiness pointBusiness;

    @ApiOperation(value = "Listado puntos de transporte")
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> obtain( @RequestParam(name = "nitEmpresa", required=false) String nitBusiness ) {
        return new ResponseEntity<>(this.pointBusiness.obtain( nitBusiness ), HttpStatus.OK);
    }

    @ApiOperation(value = "Buscar punto de transporte")
    @GetMapping(value = "/{rowKey}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> obtainByRowKey(@PathVariable(name = "rowKey") String idPoints) {
        return new ResponseEntity<>(this.pointBusiness.obtainByRowKey(idPoints), HttpStatus.OK);
    }

    @ApiOperation(value = "Crear punto de transporte")
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> create(@RequestBody PointsCreateReqDto points) {
        return new ResponseEntity<>(this.pointBusiness.save(points), HttpStatus.OK);
    }

    @ApiOperation(value = "Actualizar punto de transporte")
    @PutMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> update(@RequestBody PointsUpdateReqDto points) {
        return new ResponseEntity<>(this.pointBusiness.update(points), HttpStatus.OK);
    }

    @ApiOperation(value = "Inhabilitar punto de transporte")
    @DeleteMapping(value = "/{rowKey}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> delete(@PathVariable(name = "rowKey") String idPoints) {
        return new ResponseEntity<>(this.pointBusiness.delete(idPoints), HttpStatus.OK);
    }
}
