package com.colsubsidio.portaltransportadores.administrador.api;

import com.colsubsidio.portaltransportadores.administrador.business.BusinessBusiness;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.BusinessCreateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.BusinessUpdateReqDto;
import com.colsubsidio.portaltransportadores.administrador.commons.dto.ResponseDto;
import io.swagger.annotations.ApiOperation;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("empresas")
@RequiredArgsConstructor(onConstructor = @__(@Inject))
public class BusinessController {

    private final @NonNull BusinessBusiness businessBusiness;

    @ApiOperation(value = "Listar empresas")
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> obtain() {
        return new ResponseEntity<>(this.businessBusiness.obtain(), HttpStatus.OK);
    }

    @ApiOperation(value = "Buscar empresa")
    @GetMapping(value = "/{rowKey}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> obtain(@PathVariable(name = "rowKey") String idBusiness) {
        return new ResponseEntity<>(this.businessBusiness.obtainByRowKey(idBusiness), HttpStatus.OK);
    }

    @ApiOperation(value = "Crear empresa")
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> create(@RequestBody BusinessCreateReqDto business) {
        return new ResponseEntity<>(this.businessBusiness.saveBusiness(business), HttpStatus.OK);
    }

    @ApiOperation(value = "Actualizar empresa")
    @PutMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> update(@RequestBody BusinessUpdateReqDto business) {
        return new ResponseEntity<>(this.businessBusiness.update(business), HttpStatus.OK);
    }

    @ApiOperation(value = "Inhabilitar empresa")
    @DeleteMapping(value = "/{rowKey}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto> delete(@PathVariable(name = "rowKey") String idBusiness) {
        return new ResponseEntity<>(this.businessBusiness.delete(idBusiness), HttpStatus.OK);
    }

}
